// Declare library;
var createError = require('http-errors');
var express = require('express');
var engine = require('ejs-locals');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require("passport");

var nodalytics = require('nodalytics');

// Connect to database
var db = require('./config/connectDatabase');

// Declare database Schema
var schema = require('./config/databaseSchema');

// Declare page location;
var indexRouter = require('./routes/mainpage');
var diachido = require('./routes/diachido');
var mevnah = require('./routes/mevnah');
var admin = require('./routes/admindashboard');
var verify = require('./routes/verify');
var qrcode = require('./routes/qrcode-direct');
var api = require('./routes/apisystem');
// Remove this when release
var test = require('./routes/test');

var app = express();

// view engine setup
app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Adding cache css, js to improve page load
app.use(express.static(path.join(__dirname, 'public'), { maxAge: '30 days' }));
app.use('/favicon.ico', express.static('images/favicon.ico'));
app.set('Cache-Control', 'max-age=3000');

//express-session middleware
app.use(
  session({
    name: 'diachido_info',
    proxy: true,
    resave: true,
    secret: "diachido.info.secrect", // session secret
    resave: false,
    saveUninitialized: true,
    cookie: {
      secure: false /*Use 'true' without setting up HTTPS will result in redirect errors*/,
    }
  })
);

//PassportJS middleware
app.use(passport.initialize());
app.use(passport.session()); //persistent login sessions

app.use(flash());

// connect to passport config
require('./config/passport')(passport);

// app.use(nodalytics('UA-122822286-1'));

// Direct link
app.use('/', indexRouter);
app.use('/diachido', diachido);
app.use('/mevnah', mevnah);
app.use('/admin', admin);
app.use('/verify', verify);
app.use('/qrdirect', qrcode);
app.use('/api', api);

app.use('/test', test);

app.use('*', (req, res, next) => {
  res.render('page404', { title: "Error | 404"});
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.render('page404', { title: "Error | 404"});
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('page404', { title: "Error | 404"});
});

module.exports = app;
