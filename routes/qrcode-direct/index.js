var express = require('express');
var router = express.Router();

require('./redirect')(router);

module.exports = router;
