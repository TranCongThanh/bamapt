var express = require('express');
var config = require('../../config/index');
var mongoose = require('mongoose');
var router = express.Router();
var ua = require('universal-analytics');

const visitor = ua(config.GoogleAnalyticsID);

module.exports = router => {
  router.get('/:id', (req, res, next) => {
    let query = {
      _id: req.params.id,
      isBanned: false,
    }
    mongoose.model('qrCode').findOne(query, (err, directTo) => {
      if (err) {
        res.render('directPage', { 
          title: 'Chuyển hướng trang',
          linkDirectTo: config.SERVERURL
        });
      }
      if (directTo) {
        visitor.pageview(`${directTo.directTo}`).send();
        res.redirect(directTo.directTo);
      } else {
        res.render('directPage', { 
          title: 'Chuyển hướng trang',
          linkDirectTo: config.SERVERURL
        });
      }
    })    
  });
};
