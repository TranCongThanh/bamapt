var express = require('express');
var slugify = require('slugify');
var mongooese = require('mongoose');
var queryString = require('query-string');
var router = express.Router();

findAllDistrict = (callback) => {
  mongooese.model('districts').find({}, (err, result) => {
    if (err) throw err;
    return callback(result);
  })
}

module.exports = router => {
  router.get('/search/mevnah', (req, res, next) => {
    // let link = new url()
    let limit = 6;
    let queryObject = queryString.parseUrl(req._parsedOriginalUrl.path).query;
    let skip = queryObject.skip ? parseInt(queryObject.skip) : 0;
    var query = {
      slug: new RegExp(`${slugify(queryObject.find.toLowerCase())}`,'g'),
      districtsID: queryObject.districts === '' ? { $exists: true, $ne: null } : queryObject.districts,
      wardsID: queryObject.wards === '' ? { $exists: true, $ne: null } : queryObject.wards,
      isBanned: false,
    }
    
    console.log(query);
   
    mongooese.model('mevnah').count(query, (err, total) => {

      mongooese.model('mevnah').find(query)
        .populate('districtsID')
        .populate('wardsID')
        .populate('qrCodeID')
        .limit(limit)
        .skip(skip)
        .exec((err, mevnahList) => {
          if (err) throw err;
          console.log(mevnahList);
            if (mevnahList) {  
              skip +=limit;            
              return res.send({
                mevnahList,
                skip,
                haveOther: total <= skip ? false : true,          
              })
            } else {
              return res.send({
                haveOther: false,
              })
            }
        })
    })
    
  })
}
