var express = require('express');
var mongooese = require('mongoose');
var router = express.Router();

module.exports = router => {
  router.get('/', function(req, res, next) {
    mongooese.model('mevnah').find({ isBanned: false }).populate('qrCodeID').exec((err, mevnah) => {
      if (err) throw err;
      
      mongooese.model('mevnah').count({ isBanned: false }, (err, numberOfMevnah) => {
        if (err) throw err;
        mongooese.model('districts').find({})
        .sort({
          districtSlug: 1,
        })
        .exec((err, districtList) => {
          if (err) throw err;
          mongooese.model('cooperation').find({}, (err, cooperation) => {
            if (err) throw err;
            let numbers = Object.assign({numberOfMevnah: numberOfMevnah}, {numberOfDistricts: districtList.length});
            mongooese.model('imageVisit').find({ showHomepage: true }).populate('refMevnah').exec((err, imageVisit ) => {
              if (err) throw err;
              res.render('homepage', { 
                title: 'BA MÁ PHONG TRÀO',
                numbers,
                districtList,
                cooperation,
                mevnah,
                imageVisit
              });
            })
          })
        })
      })
    })
  });
}
