var express = require('express');
var router = express.Router();

require('./users')(router);
require('./uploadFile')(router);
// require('./diachido')(router);
require('./form')(router);
require('./getWardsList')(router);
require('./countNumberOfConnectionQRCode')(router);

module.exports = router;
