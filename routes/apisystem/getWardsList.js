var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

module.exports = router => {
  router.get('/list/:districtsID', (req, res, next) => {
    mongoose.model('wards').find({districtsID: req.params.districtsID}, (err, result) => {
      if (err) return res.send(false);
      if (result) {
        return res.send(result);
      }
    })
  })
}