var express = require('express');
var mongoose = require('mongoose');
var report = require('../../API/googleAnalytics');
var router = express.Router();

module.exports = router => {
  router.post('/qrcode/count/:id', (req, res, next) => {
    report.qrCode(req.body.startDate, 'today', [{_id: req.params.id}], result => {
      if (result) {
        res.send({
          _id: req.params.id,
          data: result.reports[0].data.rows ? result.reports[0].data.rows[0].metrics[0].values[0] : result.reports[0].data.totals[0].values[0]
        })
      } else {
        res.send(0)
      }
    })
  })
}