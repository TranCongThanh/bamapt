var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();


module.exports = router => {
 router.get('/diachido/:id', (req, res, next) => {
   if (req.isAuthenticated()) {
    mongoose.model('diachido').findById(req.params.id, (err, result) => {
      if (err) {
        console.log(err);
        return res.send(false);
      }
      if (result) 
        return res.send(result);
      return res.send(false);
    })
   } else {
     return res.send(false);
   }
 });

 router.put('/diachido/:id', (req, res, next) => {
   if (req.isAuthenticated()) {
    mongoose.model('diachido').findByIdAndUpdate(req.params.id, req.body, {new: false}, (err, result) => {
      if (err) {
        console.log(err);
        return res.send(false);
      }
      if (result) 
        return res.send(result);
      return res.send(false);
    })
   } else {
     return res.send(false);
   }
 })

 router.delete('/diachido/:id', (req, res, next) => {
   if (req.isAuthenticated()) {
     let update = {
       isBanned: true,
     }
    mongoose.model('diachido').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
      if (err) {
        console.log(err);
        return res.send(false);
      }
      if (result) 
        return res.send(result);
      return res.send(false);
    })
   } else {
     return res.send(false)
   }
 })

 router.put('/diachido/:id/active', (req, res, next) => {
  if (req.isAuthenticated()) {
    let update = {
      isBanned: false,
    }
   mongoose.model('diachido').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
     if (err) {
       console.log(err);
       return res.send(false);
     }
     if (result) 
       return res.send(result);
     return res.send(false);
   })
  } else {
    return res.send(false);
  }
})

};
