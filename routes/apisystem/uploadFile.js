var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var uploadFile = require('../../API/uploadFile');


module.exports = router => {

  var uploadImage = uploadFile.StoreFile('images');
  // To call this function use method post to /api/upload/image
  router.post('/upload/image', uploadImage.single('image'), (req, res, next) => {
    const fileId = req.file.filename.split('.')[0];
    const endFile = req.file.filename.split('.')[1];
    // Remove public in destination and add filename in the link
    req.file.link = req.file.destination.substring(6, req.file.destination.length) + req.file.filename;
    if (req.isAuthenticated()) {
      uploadFile.updateUser(req.user, fileId, next => {
        if (next) {
          uploadFile.compressImage(fileId, endFile , 'images');
          return res.send({
            id: fileId,
            link: req.file.link,
            name: req.file.originalname,
          })
        }
        return res.send(false);
      });
    } else {
      uploadFile.removeFile(fileId);
      return res.send(false);
    }
  })
  // This api is used to update field reference to post (diachido or mevnah) after submit post
  router.post('/upload/image/:id/:type', (req, res, next) => {
    if (req.isAuthenticated()) {
      let update = {
        isDiachido: req.params.type == 'diachido' ? true : false,
        refMevnah: req.params.type == 'mevnah' ? req.params.id : null,
        refDiachido: req.params.type == 'diachido' ? req.params.id : null,
      }
      let option = { new: false }
      mongoose.model('objectUpload').findByIdAndUpdate(req.params.id, update, option, (err, reuslt) => {
        if (err) {
          console.log(err);
          return res.send(false);
        }
        if (result) {
          return res.send(true);
        }
        return res.send(false);
      })
    } else {
      return res.send(false);
    }
  })

  router.delete('/remove/image/:id', (req, res, next) => {
    if (req.isAuthenticated()) {
      mongoose.model('objectUpload').findById(req.params.id, (err, result) => {
        if (err) {
          console.log(err);
          return res.send(false);
        }
        if (result) {
          uploadFile.removeFile(req.params.id)
          return res.send(true);
        }
        return res.send(false);
      });
    } else {
      uploadFile.removeFile(fileId, req.file.link);
      return res.send(false);
    }
  })

};
