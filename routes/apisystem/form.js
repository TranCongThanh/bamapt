var express = require('express');
var mongoose = require('mongoose');
var sendEmail = require('../../API/sendEmail');
var ejs = require('ejs');
var createLog = require('../../API/createLog');
var router = express.Router();

receivedInfo = (body) => {
  ejs.renderFile(__dirname + '/emailTemplate/replyUsers.ejs', {
    name: body.name,
    title: body.title,
    content: body.content,
  }, (err, html) => {
    if (err) throw err;
    sendEmail.Send(body.email, `[diachido.info] - Tiếp nhận phản hồi từ bạn ${body.name}`, html)
    // createLog.newReply('reply', body.name, body.content)
  })
}

alertNewContact = (body) => {
  let query = {
    $or: [
      {
        level: 3,
        districtsID: body.refDiachido ? body.refDiachido.districtsID : body.refMevnah.districtsID,
        wardsID: body.refDiachido ? body.refDiachido.wardsID : body.refMevnah.wardsID,
      },
      {
        level: 2,
        districtsID: body.refDiachido ? body.refDiachido.districtsID : body.refMevnah.districtsID,
      }
    ]
  }
  mongoose.model('users').find(query, (err, info) => {
    if (err) throw err;
    if (info) {
      ejs.renderFile(__dirname + '/emailTemplate/receivedNewFeedBack.ejs', {
        info: body.refDiachido ? body.refDiachido.name : body.refMevnah.name,
        address: body.refDiachido ? body.refDiachido.address : body.refMevnah.address,
        type: body.type,
        name: body.name,
        email: body.email,
        title: body.title,
        content: body.content,
      }, (err, html) => {
        if (err) throw err;
        info.map((item, index) => {
          sendEmail.Send(item.email, `[diachido.info] - Thông tin phản hồi mới từ ${body.name}`, html)
          // createLog.newReply('reply', body.name, body.content)
        })
      })
    }
  })
}

alertNewContactMainPage = (body) => {
  let query = {
    $or: [
      {
        level: 1,
      },
      {
        isDeveloper: true
      }
    ]
  }
  mongoose.model('users').find(query, (err, info) => {
    if (err) throw err;
    if (info) {
      ejs.renderFile(__dirname + '/emailTemplate/receivedNewFeedBack.ejs', {
        info: 'Mainpage',
        address: 'Mainpage',
        type: body.type,
        name: body.name,
        email: body.email,
        title: body.title,
        content: body.content,
      }, (err, html) => {
        if (err) throw err;
        info.map((item, index) => {
          sendEmail.Send(item.email, `[diachido.info] - Thông tin phản hồi mới từ ${body.name}`, html)
          // createLog.newReply('reply', body.name, body.content)
        })
      })
    }
  })
}

updateDistrictAndWardField = (id, districtsID, wardsID) => {
  let update = {
    districtsID,
    wardsID
  }
  mongoose.model('replyFromUsers').findByIdAndUpdate(id, update, {new: false})
  .populate('refDiachido')
  .populate('refMevnah')
  .exec((err, result) => {
    if (err) throw err;
    if (result) {
      alertNewContact(result)
      return true;
    }
    return false;
  })
}

module.exports = router => {
 router.post('/form/:type/:id', (req, res, next) => {
  if (req.params.type == 'diachido' || req.params.type == 'mevnah') {
    let insert = {
      email: req.body.email,
      name: req.body.name,
      type: req.params.type == 'diachido' ? 'diachido' : 'mevnah',
      refMevnah: req.params.type == 'diachido' ? null : req.params.id,
      refDiachido: req.params.type == 'diachido' ? req.params.id : null,
      title: req.body.title,
      content: req.body.content,
    }
    mongoose.model('replyFromUsers').create(insert, (err, result) => {
      if (err) throw err;
      mongoose.model('replyFromUsers').findById(result.id)
      .populate({
        path: 'refDiachido',
        populate: {
          path: 'wardsID',
        }
      })
      .populate({
        path: 'refMevnah',
        populate: {
          path: 'wardsID',
        }
      })
      .exec((err, info) => {
        if (err) {
          return res.send(false)
        };
        if (info) {
          // After create document for this database, this code below query this again but
          // Combine with populate to get fields districtsID and wardsID and update it
          if (info.type == 'diachido') {
            let districtsID = info.refDiachido.wardsID ? info.refDiachido.wardsID.districtsID : null;
            let wardsID = info.refDiachido.wardsID ? info.refDiachido.wardsID._id : null;
            updateDistrictAndWardField(result._id, districtsID, wardsID);
          } else {
            let districtsID = info.refMevnah.wardsID ? info.refMevnah.wardsID.districtsID : null;
            let wardsID = info.refMevnah.wardsID ? info.refMevnah.wardsID._id : null;
            updateDistrictAndWardField(result._id, districtsID, wardsID);
          }
          receivedInfo(req.body);
          return res.send(result);
        }
        return res.send(false);
      })
    })
    
  } else {
    return res.send(false);
  }
 })

 router.post('/form/mainpage', (req, res,next) => {
   let insert = {
     email: req.body.email,
     name: req.body.name,
     type: 'mainpage',
     title: req.body.title,
     content: req.body.content,
    }
  mongoose.model('replyFromUsers').create(insert, (err, result) => {
    if (err) {
      return res.send(false)
    };
    
    if (result) {
      receivedInfo(req.body);
      alertNewContactMainPage(result);
      return res.send(true);
    } else {
      res.send(false);
    }
  })
 })
};
