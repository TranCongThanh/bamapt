var express = require("express");
var router = express.Router();

var diachido = { name: "ĐỊA CHỈ ĐỎ" };

var gioithieu = [
  {
    tittle: "Mo ta",
    content:
      "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  },
  {
    tittle: "Mo ta 1",
    content:
      "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  },
  {
    tittle: "Mo ta 2",
    content:
      "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  },
  {
    tittle: "Mo ta 3",
    content:
      "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  }
];

var hinhanh = [
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  },
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  },
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  },
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  },
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  },
  {
    tittle: "Hinh 1",
    src: "https://i.quantrimang.com/photos/image/2018/01/10/hinh-nen-4k-6.jpg"
  }
];

var giaidoan = [
  {
    year: "1930-1945",
    content:
      "Thiên Hộ Võ Duy Dương (1827-1866), còn gọi là Thiên Hộ Dương (do giữ chức Thiên hộ), là lãnh tụ cuộc khởi nghĩa chống thực dân Pháp (1862-1866) ở vùng Đồng Tháp Mười, thuộc Đồng bằng Sông Cữu Long, Việt Nam."
  },
  {
    year: "1945-1960",
    content:
      "Thiên Hộ Võ Duy Dương (1827-1866), còn gọi là Thiên Hộ Dương (do giữ chức Thiên hộ), là lãnh tụ cuộc khởi nghĩa chống thực dân Pháp (1862-1866) ở vùng Đồng Tháp Mười, thuộc Đồng bằng Sông Cữu Long, Việt Nam."
  },
  {
    year: "1960-1975",
    content:
      "Thiên Hộ Võ Duy Dương (1827-1866), còn gọi là Thiên Hộ Dương (do giữ chức Thiên hộ), là lãnh tụ cuộc khởi nghĩa chống thực dân Pháp (1862-1866) ở vùng Đồng Tháp Mười, thuộc Đồng bằng Sông Cữu Long, Việt Nam."
  },
  {
    year: "1975-Today",
    content:
      "Thiên Hộ Võ Duy Dương (1827-1866), còn gọi là Thiên Hộ Dương (do giữ chức Thiên hộ), là lãnh tụ cuộc khởi nghĩa chống thực dân Pháp (1862-1866) ở vùng Đồng Tháp Mười, thuộc Đồng bằng Sông Cữu Long, Việt Nam."
  }
];

var quanli = {
  name: "BAN QUẢN LÍ",
  address: "Số 1 Phạm Ngọc Thạch, Quận 1, TP HCM",
  phone: "0123456789",
  mail: "laptrinhweb@uit.edu.vn"
};

var admin = {
  phone: "0123456789",
  mail: "laptrinhweb@uit.edu.vn"
};


module.exports = router => {
  
  require('./showAllDistricts')(router);
  require('./objectsDetail')(router);

};