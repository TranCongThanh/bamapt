var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();


module.exports = router => {
  // This page show all districts have available object
  router.get("/:districtSlug/:wardSlug/:objectSlug", function(req, res, next) {
    mongoose.model('districts').findOne({districtSlug: req.params.districtSlug}, (err, districtInfo) => {
      if (err) throw err;
      let query = {
        districtsID: districtInfo._id,
        wardSlug: req.params.wardSlug,
      }
      console.log('query', query)
      mongoose.model('wards').findOne(query, (err, wardInfo) => {
        if (err) throw err;
        let query = {
          districtsID: districtInfo._id,
          wardsID: wardInfo._id,
          slug: req.params.objectSlug,
        }
        mongoose.model('diachido').findOne(query, (err, diachidoInfo) => {
          if (err) throw err;
          res.render('diachido/home/index', {
            objectInfo: diachidoInfo,
          })
        })
      })
    })
  });

};
