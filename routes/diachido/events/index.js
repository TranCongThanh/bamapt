var express = require('express');
var router = express.Router();

module.exports = router => {
  router.get('/events', function(req, res, next) {
    res.render('landingpage/events');
  });

};
