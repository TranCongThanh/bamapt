var express = require('express');
var router = express.Router();

// Include homepage
require('./home/index')(router);
require('./events/index')(router);
require('./history/index')(router);

module.exports = router;
