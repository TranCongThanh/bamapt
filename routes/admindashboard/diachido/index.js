var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  require('./showAllObjects')(router);
  // require('./updateObject')(router);
  require('./create-objects')(router);
  require('./bannedAndActive')(router);
  require('./create-notification')(router);
  require('./edit-object')(router);

};
