// var express = require("express");
// var slugify = require('slugify');
// var mongoose = require("mongoose");
// var config = require("../../../config/index");
// var qrCode = require("../../../API/createQRCode");
// var router = express.Router();

// module.exports = router => {
//   router.put("/diachido/:id", (req, res, next) => {

//     if (req.isAuthenticated()) {
//       var {
//         objectName,
//         districtID,
//         wardID,
//         address,
//         background,
//         description,
//         stages,
//         someOfOtherImage,
//         numberPhoneContact,
//         emailContact,
//         addressContact
//       } = req.body;

//       background = JSON.parse(background);
//       description = JSON.parse(description);
//       stages = JSON.parse(stages);
//       someOfOtherImage = JSON.parse(someOfOtherImage);

//       let insert = {
//         name: objectName,
//         slug: slugify(objectName.toLowerCase()),
//         address: address,
//         postedBy: req.user._id,
//         districtsID: districtID,
//         wardsID: wardID,
//         page: {
//           description: description,
//           stages: stages,
//           background: background,
//           someOfOtherImage: someOfOtherImage,
//         },
//         contact: {
//           numberPhoneContact: numberPhoneContact,
//           emailContact: emailContact,
//           addressContact: addressContact,
//         }
//       }
//       mongoose.model('diachido').create(insert, (err, result) => {
//         if (err) throw err;
//         if (result) {
//           res.send(true);
//           mongoose.model('diachido').findById(result._id)
//             .populate("districtsID")
//             .populate("wardsID")
//             .exec((err, diachidoInfo) => {
//               if (err) throw err;
//               let districtSlug = diachidoInfo.districtsID ? diachidoInfo.districtsID.districtSlug : null;
//               let wardSlug = diachidoInfo.wardsID ? diachidoInfo.wardsID.wardSlug : null;
//               let link = `${config.SERVERURL}/diachido/${districtSlug}/${wardSlug}/${diachidoInfo.slug}`;
//               qrCode.CreateAndSave('diachido', link, result._id ,qrLink => {
//                 let update = {
//                   imageQRLink: qrLink,
//                 }
//                 mongoose.model('diachido').findByIdAndUpdate(result._id, update, {new: false}, (err, finalResult) => {
//                   if (err) throw err;
//                 })
//               });
//             })
//         }
//       })
      
//     } else {
//       // res.redirect('/admin/create-object');
//       return res.send(false);
//     }
//   });
// };
