var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/diachido/:id", function(req, res, next) {
    if (req.isAuthenticated()) {
      mongoose.model('diachido')
      .findById(req.params.id)
      .populate('users')
      .populate('districtsID')
      .populate('wardsID')
      .populate('qrCodeID')
      .exec((err, info) => {
        if (err) {
          console.log(err);
          return res.send(false);
        }
        if (info) {
          return res.send(info);
        }
      })
      
    } else {
      return res.send(false);
    }
  });

};
