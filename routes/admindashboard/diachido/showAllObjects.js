var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");
var config = require("../../../config/index");
var qrCode = require("../../../API/createQRCode");
var router = express.Router();

module.exports = router => {
  router.get("/diachido/listAllObjects", function(req, res, next) {
    if (req.isAuthenticated()) {
      if (req.user.level == 1) {
        var query = {};
      }
      if (req.user.level == 2) {
        var query = {
          districtsID: req.user.districtsID,
        };
      }
      if (req.user.level == 3) {
        var query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID
        };
      }
      mongoose
        .model("diachido")
        .find(query)
        .populate("wardsID")
        .populate("districtsID")
        .populate("qrCodeID")
        .exec((err, diachidoList) => {
          if (err) throw err;
          if (diachidoList) {
            res.render("admindashboard/diachido/index", {
              title: "Địa chỉ đỏ | Admin",
              parent_directory: "Địa chỉ đỏ",
              directory: "Tất cả địa chỉ đỏ",
              diachidoList: diachidoList
            });
          }
        });
    } else {
      res.redirect('/admin/login');
    }
  });
};
