var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

bannedAndActive = (qrCodeID, newStatus) => {
  mongoose.model('qrCode').findByIdAndUpdate(qrCodeID, {isBanned: newStatus}, {new: false}, (err, result) => {
    if (err) {
      console.log('err', err);
      return false;
    }
    if (result) {
      return true;
    }
    return false;
  })
}

module.exports = router => {

  // Banned Objects
  router.delete('/diachido/:id', (req, res, next) => {
   if (req.isAuthenticated()) {
     let update = {
       isBanned: true,
     }
    mongoose.model('diachido').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
      if (err) {
        console.log(err);
        return res.send(false);
      }
      if (result) {
        // Banned qrCode too
        if (bannedAndActive(result.qrCodeID, true)) {
          return res.send(result);
        } else {
          return res.send(false);
        }
      }
      return res.send(false);
    })
   } else {
     return res.send(false)
   }
 })

 // Active Objects
  router.put('/diachido/:id/active', (req, res, next) => {
    if (req.isAuthenticated()) {
      let update = {
        isBanned: false,
      }
    mongoose.model('diachido').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
      if (err) {
        console.log(err);
        return res.send(false);
      }
      if (result) {
        // Active qrCode too
        if (bannedAndActive(result.qrCodeID, false)) {
          return res.send(result);
        } else {
          return res.send(false);
        }
      }
      return res.send(false);
    })
    } else {
      return res.send(false);
    }
  })

};
