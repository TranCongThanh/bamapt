var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/diachido/create-notification/:id", function(req, res, next) {
    if (req.isAuthenticated()) {
      let query = {
        refDiachido: req.params.id
      }
      mongoose.model('notification').findOne(query, (err, result) => {
        if (err) throw err;
        // If this object hasn't created notification, create new
        // If did, push new to description
        if (!result) {
          mongoose.model('diachido').findById(req.params.id, (err, diachidoInfo) => {
            if (err) throw err;
            if (diachidoInfo) {
              let {
                name,
                timeHappends,
                content,
                timeShow,
                timeEnd,
                background
              } = req.body;
              let insert = {
                isDiachido: true,
                refDiachido: result._id,
                districtsID: diachidoInfo.districtsID,
                wardsID: diachidoInfo.wardsID,
                description: [{
                  name: name,
                  timeHappends: timeHappends,
                  content: content,
                  timeShow: timeShow,
                  timeEnd: timeEnd,
                  background: background
                }]
              }
              mongoose.model('notification').create(insert, (err, notifi) => {
                if (err) throw err;
                if (notifi)
                  return res.send(notifi)
                else return res.send(false);
              })
            }
          })
        } else {
          let {
            name,
            timeHappends,
            content,
            timeShow,
            timeEnd,
            background
          } = req.body;
          let update = {
            $push: {
              description: {
                name: name,
                timeHappends: timeHappends,
                content: content,
                timeShow: timeShow,
                timeEnd: timeEnd,
                background: background
              }
            }
          }
          mongoose.model('notification').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
            if (err) throw err;
            if (result) 
              return res.send(result);
            else return res.send(false);
          })
        }
      })
    } else {
      return res.send(false); 
    }
  });

};
