var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  // Redirect users connect to /image to page 0
  router.get("/image", (req, res, next) => {
    if (req.isAuthenticated()) {
      res.redirect('/admin/image/page=0&number=12');
    } else {
      res.redirect('/admin/login')
    }
  })

  router.get("/image/:queryParams", function(req, res, next) {
    // Example = ?page=0&number=12
    const queryParams = req.params.queryParams;
    // Get Page
    const page = parseInt(queryParams.substring(queryParams.indexOf('=') + 1, queryParams.indexOf('&')));
    // Get Number
    const number =parseInt(queryParams.substring(queryParams.lastIndexOf('=') + 1, queryParams.length));
    if (req.isAuthenticated()) {
      var query = {};
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
        };
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID
        };
      }
      mongoose
        .model("objectUpload")
        .find(query)
        .skip(page * number)
        .limit(number)
        .sort({
          timeUpload: 'desc'
        })
        .populate("refDiachido", "refMevnah")
        .exec((err, imagesList) => {
          if (err) throw err;
          if (imagesList) {
            mongoose.model("objectUpload").count(query, (err, numberOfImage) => {
              if (err) throw err;
              res.render("admindashboard/image", {
                title: "Hình ảnh | Admin",
                parent_directory: "Media",
                directory: "Hình ảnh",
                imagesList: imagesList,
                currentPage: page + 1,
                numberOfPage: parseInt(Math.ceil(numberOfImage / number)),
                numberOfImage: numberOfImage,
                number: number,
                level: req.user.level,
                isDeveloper: req.user.isDeveloper,
              });
            })
          }
        });
    } else {
      res.redirect('/admin/login');
    }
  });
};
