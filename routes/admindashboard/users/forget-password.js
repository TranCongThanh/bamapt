var express = require('express');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var ejs = require('ejs');
var sendEmail = require('../../../API/sendEmail');
var config = require('../../../config/index');

var router = express.Router();

module.exports = router => {
  router.post('/forget-password', (req, res, next) => {
    mongoose.model('users').findOne({ email: req.body.email }, (err, hander) => {
      if (err) throw err;

      if (hander != null) {
        let token = randomString.generate(50);
        let insert = {
          user: hander._id,
          email: hander.email,
          token: token,
        };
        mongoose.model('forgetPassword').create(insert, (err, result) => {
          if (err) throw err;
          // Send email to users
          let linkVerify = config.SERVERURL + '/verify/forget-password/' + result.user + '/' + token;
          ejs.renderFile(__dirname + '/emailTemplate/forget-password-template.ejs', {
            linkVerify: linkVerify,
            name: hander.name,
          }, (err, html) => {
            if (err) throw err;
            sendEmail.Send(hander.email, '[[bamaphongtraohssv.vn]] - Xác nhận khôi phục mật khẩu', html);
            res.render('admindashboard/users/forget-password', { 
              title: "Quên mật khẩu | Admin",
              mess: 'Hãy kiểm tra email để tạo lại mật khẩu mới',
            })
          })
        })
      } else {
        res.render('admindashboard/users/forget-password', { 
          title: "Quên mật khẩu | Admin",
          mess: 'Email bạn nhập không tồn tại trong hệ thống',
        })
      }
    })
  })
}