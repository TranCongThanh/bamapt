var express = require('express');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var ejs = require('ejs');
var sendEmail = require('../../../API/sendEmail');
var config = require('../../../config/index');

var router = express.Router();

module.exports = router => {
  router.post('/change-password', (req, res, next) => {
    if (req.isAuthenticated()){
      mongoose.model('users').findOne({ username: req.user.username }, (err, result) => {
        bcrypt.compare(req.body.oldPassword, result.password, (err, isMatch) => {
          if (err) throw err;
          if (isMatch) {
            // Compare newPassword with reTypePassword
            if (req.body.newPassword === req.body.reTypePassword) {
              bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
                let query = { username: req.user.username };
                let update = { password: hash };
                let option = { new: false };
                mongoose.model('users').findOneAndUpdate(query, update, option, (err, hander) => {
                  if (err) throw err;
                  req.logout();
                  res.redirect('/admin/login');
                })
              })
            } else {
              res.render('admindashboard/users/change-password', { 
                title: "Đổi mật khẩu | Admin",
                mess: "Mật khẩu mới không khớp nhau, vui lòng nhập lại",
              })
            }
          } else {
            // Type wrongpassword
            res.render('admindashboard/users/change-password', { 
              title: "Đổi mật khẩu | Admin",
              mess: "Bạn đã nhập sai mật khẩu cũ, vui lòng nhập lại"
            })
          }
        })
      })
    } else res.redirect('/admin/login');
  });
}