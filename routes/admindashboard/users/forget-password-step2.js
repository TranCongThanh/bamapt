var express = require('express');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');

var router = express.Router();

module.exports = router => {
  router.post('/change-password/:token', (req, res, next) => {
    if (req.body.newPassword === req.body.reTypePassword) {
      let query = {
        token: req.params.token,
        acceptToken: true,
      }
      let update = { isActive: true }
      let option = { new: false }
      mongoose.model('forgetPassword').findOneAndUpdate(query, update, option, (err, result) => {
        bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
          let query = { _id: result.user };
          let update = { password: hash };
          let option = { new: false };
          mongoose.model('users').findOneAndUpdate(query, update, option, (err, hander) => {
            if (err) throw err;
            res.redirect('/admin/login');
          })
        })
      })
    } else {
      res.render('admindashboard/users/new-password', { 
        title: "Đổi mật khẩu | Admin",
        mess: 'Hai mật khẩu không khớp nhau, vui lòng nhập lại!',
        token: token,
      })
    }
    
  });
}