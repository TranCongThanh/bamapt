var express = require('express');
var passport = require('passport');
var router = express.Router();


module.exports = router => {

  // Method GET
  router.get('/login', function(req, res, next) {
    res.render('admindashboard/users/login', { title: "Đăng nhập | Admin", mess: ''});
  });

  router.get('/login/wrongpass', function(req, res, next) {
    res.render('admindashboard/users/login', { title: "Đăng nhập | Admin", mess: 'Bạn đã nhập sai tài khoản hoặc mật khẩu!'});
  });

  //Method POST
  router.post("/login", 
    passport.authenticate("local", {
      failureRedirect: "/admin/login/wrongpass",
      failureFlash: false
      }), (req, res) => {
        if (req.body.remember) {
          req.session.cookie.maxAge = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000); // Cookie expires after 30 days
        } else {
          req.session.cookie.expires = false; // Cookie expires at end of session
        }
        res.redirect('/admin');
      }
  );

  router.get('/change-password', function(req, res, next){
    res.render('admindashboard/users/change-password', { title: "Đổi mật khẩu | Admin", mess: ''})
  });

  router.get('/forget-password', function(req, res, next){
    res.render('admindashboard/users/forget-password', { 
      title: "Quên mật khẩu | Admin",
      mess: '',
    })
  });

  router.get('/logout', (req, res, next) => {
    req.logout();
    res.redirect('/admin/login');
  })

  require('./allUsers')(router);

  // Get page register
  require('./register')(router);

  // Change Password
  require('./change-password')(router);

  // Forget Password
  require('./forget-password')(router);

  // New Password after forget Password
  require('./forget-password-step2')(router);

  require('./resetPassword')(router);

  require('./profileUser')(router);

};
