var express = require("express");
var bcrypt = require("bcryptjs");
var mongoose = require("mongoose");
var randomString = require("randomstring");
var ejs = require("ejs");
var sendEmail = require("../../../API/sendEmail");
var config = require("../../../config/index");

var router = express.Router();

module.exports = router => {
  router.get("/account", function(req, res, next) {
    res.render("admindashboard/users/profileUser", {
      title: "Người dùng | Admin",
      parent_directory: "Người dùng",
      directory: "Thông tin người dùng",
      level: req.user.level,
      isDeveloper: req.user.isDeveloper,
    });
  });
};
