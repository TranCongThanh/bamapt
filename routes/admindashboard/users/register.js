var express = require('express');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var ejs = require('ejs');
var sendEmail = require('../../../API/sendEmail');
var config = require('../../../config/index');

var router = express.Router();

module.exports = router => {
  router.get('/register', function(req, res, next) {
    if (req.isAuthenticated() && req.user.level <= 2) {
      mongoose.model('users').findById(req.user._id, (err, result) => {
        if (err) throw err;
        // If user is admin
        if (result.level == 1) {
          // Search list of district
          mongoose.model('districts').find({}, (err, districtList) => {
            if (err) throw err;
            if (districtList) {
              res.render('admindashboard/users/register', { 
                title: "Đăng ký | Admin",
                prepend: 'district_',
                levelList: districtList,
                levelNextName: 'Quận',
                isDeveloper: req.user.isDeveloper,
                postTo: '/admin/create-user'
              });
            } else {
              res.redirect('/admin');
            }
          })
        }
        // If user is a district's user
        else if (result.level == 2) {
          let query = { districtsID: req.user.districtsID }
          // Search list of ward in that district
          mongoose.model('wards').find(query, (err, wardList) => {
            if (err) throw err;
            if (wardList) {
              res.render('admindashboard/users/register', { 
                title: "Đăng ký | Admin",
                prepend: 'ward_',
                levelNextName: 'Phường',
                levelList: wardList,
                isDeveloper: req.user.isDeveloper,
                postTo: `/admin/create-user/${req.user.districtsID}`,
              });
            }
          })
        }
      })
    } else res.redirect('/admin/login')
  });
  
  // This is create-user for district
  router.post('/create-user', (req, res, next) => {
    if (req.isAuthenticated() && req.user.level == 1) {
      // console.log(req.body)
    var passwordNewUser = randomString.generate(15);
    bcrypt.hash(passwordNewUser, 10, (err, passHash) => {
      let insert = {
        username: 'district_' + req.body.username,
        password: passHash,
        email: req.body.email,
        name: req.body.fullname,
        level: req.body.isAdmin == 'on' ? 1 : 2,
        isDeveloper: req.body.isDeveloper == 'on' ? true : false,
        districtsID: req.body.isAdmin == 'on' ? null : req.body.district_,
      }
      mongoose.model('users').create(insert, (err, result) => {
        if (err) console.log('err', err)
          ejs.renderFile(__dirname + '/emailTemplate/create-new-user.ejs', {
            loginLink: config.SERVERURL + '/admin/login',
            password: passwordNewUser,
            email: result.email,
            ownerName: req.user.name,
            name: result.name,
          }, (err, html) => {
            if (err) throw err;
            // Send html to user
            sendEmail.Send(result.email, '[bamaphongtraohssv.vn] - Tài khoản đăng nhập', html);
            res.redirect('/admin');
          })
      })
    })
    } else res.redirect('/admin/login');
  });

  router.post('/create-user/:districtsID', (req, res, next) => {
    if (req.isAuthenticated() && req.user.level == 2) {
      // console.log(req.body)
    var passwordNewUser = randomString.generate(10);
    bcrypt.hash(passwordNewUser, 10, (err, passHash) => {
      let insert = {
        username: 'ward_' + req.body.username,
        password: passHash,
        email: req.body.email,
        name: req.body.fullname,
        level: 3,
        isDeveloper: req.body.isDeveloper,
        districtsID: req.params.districtsID,
        wardsID: req.body.ward_
      }
      mongoose.model('users').create(insert, (err, result) => {
        if (err) throw err;
          ejs.renderFile(__dirname + '/emailTemplate/create-new-user.ejs', {
            loginLink: config.SERVERURL + '/admin/login',
            password: passwordNewUser,
            email: result.email,
            ownerName: req.user.name,
            name: result.name,
          }, (err, html) => {
            if (err) throw err;
            // Send html to user
            sendEmail.Send(result.email, '[bamaphongtraohssv.vn] - Tài khoản đăng nhập', html)
            res.redirect('/admin');
          })
      })
    })
    } else res.redirect('/admin/login');
  });
}