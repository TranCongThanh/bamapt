var express = require('express');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var ejs = require('ejs');
var sendEmail = require('../../../API/sendEmail');
var config = require('../../../config/index');

var router = express.Router();

module.exports = router => {
  router.get('/users', function(req, res, next){
    if (req.isAuthenticated()) {
      if (req.user.level == 1) {
        var query = {};
      }

      if (req.user.level == 2) {
        var query = {
          districtsID: req.user.districtsID,
        }
      }

      if (req.user.level == 3) {
        var query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
        }
      }
      mongoose.model('users').find(query)
      .populate('districtsID')
      .populate('wardsID')
        .exec((err, userList) => {
          if (err) throw err;
          console.log(userList)
          res.render('admindashboard/users/allUsers', { 
            title: "Tất cả Người dùng | Admin", 
            parent_directory: "Người dùng", 
            directory: "Tất cả Người dùng",
            userList: userList,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          })
        })
    } else {
      res.redirect('/admin/login')
    }
  });
}