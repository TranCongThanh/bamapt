var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");
var list = require('../../../API/sendDistrictsAndWardsList');
var router = express.Router();

module.exports = router => {
  router.get("/mevnah/:id/edit", function (req, res, next) {

    if (req.isAuthenticated()) {

      mongoose.model('mevnah')
        .findById(req.params.id)
        .populate('users')
        .populate('districtsID')
        .populate('wardsID')
        .populate('qrCodeID')
        .exec((err, info) => {
          if (err) {
            console.log(err);
            return res.send(false);
          }
          if (info) {
            console.log(info);
            list.SendMeList(req.user.level, req.user.districtsID, req.user.wardsID, list => {
              if (req.user.level == 1) {
                res.render("admindashboard/mevnah/edit-mevnah", {
                  title: "Chỉnh sửa thông tin BMPT | Admin",
                  parent_directory: "BA MÁ PHONG TRÀO",
                  directory: "Chỉnh sửa thông tin BMPT",
                  districtList: list,
                  wardList: [],
                  mevnah: info,
                  level: req.user.level,
                  isDeveloper: req.user.isDeveloper,
                });
              }
              else if (req.user.level == 2) {
                let identifyDistrict = []
                identifyDistrict.push(list[0])
                res.render("admindashboard/mevnah/edit-mevnah", {
                  title: "Chỉnh sửa thông tin BMPT | Admin",
                  parent_directory: "BA MÁ PHONG TRÀO",
                  directory: "Chỉnh sửa thông tin BMPT",
                  districtList: identifyDistrict,
                  wardList: list[1],
                  mevnah: info,
                  level: req.user.level,
                  isDeveloper: req.user.isDeveloper,
                });
              }
              else if (req.user.level == 3) {
                let identifyDistrict = []
                let identifyWard = []
                identifyDistrict.push(list.districtsID)
                identifyWard.push(list)
                res.render("admindashboard/mevnah/edit-mevnah", {
                  title: "Chỉnh sửa thông tin BMPT | Admin",
                  parent_directory: "BA MÁ PHONG TRÀO",
                  directory: "Chỉnh sửa thông tin BMPT",
                  districtList: identifyDistrict,
                  wardList: identifyWard,
                  mevnah: info,
                  level: req.user.level,
                  isDeveloper: req.user.isDeveloper,
                });
              }
            })
          }
        })
    } else {
      return res.send(false);
    }
  });

  router.put("/mevnah/:id/edit", function (req, res, next) {
    if (req.isAuthenticated()) {
      console.log('req.body put', req.body);
      var currentTime = new Date();
      var {
        name,
        nickName,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        healthStatus,
        dayOfDeath,
        monthOfDeath,
        yearOfDeath,
        locationTomb,
        districtsID,
        wardsID,
        address,
        location,
        locatTomb,
        homeTown,
        martyrs,
        background,
        motherImageLink,
        victory,
        story,
        stages,
        prize,
        someOfOtherImage,
        nameContact,
        numberPhoneContact,
        emailContact,
        addressContact,
      } = req.body;

      background = JSON.parse(background);
      motherImageLink = JSON.parse(motherImageLink);
      story = JSON.parse(story);
      stages = JSON.parse(stages);
      prize = JSON.parse(prize);
      someOfOtherImage = JSON.parse(someOfOtherImage);
      martyrs = JSON.parse(martyrs);
      victory = JSON.parse(victory);
      location = JSON.parse(location);
      locatTomb = JSON.parse(locatTomb);
      var update = {
        name: name,
        nickName: nickName,
        address: {
          address: address,
          lat: location.lat,
          long: location.lng
        },
        postedBy: req.user._id,
        districtsID: districtsID,
        wardsID: wardsID,
        page: {
          homeTown: homeTown,
          birthday: {
            date: dayOfBirth,
            month: monthOfBirth,
            year: yearOfBirth,
          },
          healthStatus: healthStatus,
          dateOfDeath: {
            date: dayOfDeath,
            month: monthOfDeath,
            year: yearOfDeath
          },
          locationTomb: {
            address: locationTomb,
            lat: locatTomb.lat,
            long: locatTomb.lng
          },
          background: background,
          motherImageLink: motherImageLink,
          story: story,
          stages: stages,
          victory: victory,
          prize: prize,
          someOfOtherImage: someOfOtherImage,
        },
        martyrs: martyrs,
        contact: {
          numberPhoneContact: numberPhoneContact,
          emailContact: emailContact,
          addressContact: addressContact,
          nameContact: nameContact,
        }
      }
      console.log('update', update);
      mongoose.model('mevnah').findByIdAndUpdate(req.params.id, update, { new: false }, (err, mevnahInfo) => {
        if (err) {
          console.log('err', err);
          return res.send(false)
        }
        if (mevnahInfo) {
          return res.send(true);
        }
        return res.send(false);
      })
    } else {
      return res.send(false);
    }
  });
};
