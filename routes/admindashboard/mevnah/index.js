var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {

  require("./create-mevnah")(router);
  require("./showAllMevnah")(router);
  require("./bannedAndActive")(router);
  require("./create-notification")(router);
  require("./edit-mevnah")(router);
  require("./showAllNotification")(router);
  require("./editNotification")(router);
  require("./create-post")(router);
};
