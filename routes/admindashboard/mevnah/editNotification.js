var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.put("/mevnah/notification/:objectID/:id", function(req, res, next) {
    if(req.isAuthenticated()) {
      let {
        name,
        content,
        timeShow,
        timeEnd,
        background
      } = req.body;
      let query = {
        _id: req.params.objectID,
        description: {
          _id: req.params.id,
        }
      }
      background = JSON.parse(background);
      let update = {       
        $set: {
          description: {
            name: name,
            content: content,
            timeShow: timeShow,
            timeEnd: timeEnd,
            background: background
          }
        }
      }
      mongoose.model('notification').findByIdAndUpdate(query, update, {new: false}, (err, result) => {
        if (err) return res.send(false);
        if (result) {
          return res.send(true);
        }
      })
    } else {
      return res.send(false);
    }
  })

  router.get("/mevnah/notification/:objectID/:id", function(req, res, next) {
    if(req.isAuthenticated()) {
    
      let query = {
        _id: req.params.objectID,
        'description._id': req.params.id,
      }
      mongoose.model('notification').findOne(query, (err, result) => {
        if (err) return res.send(false);
        if (result) {
          return result.description.map((item, index) => {
            if (item._id == req.params.id)
              return res.send({
                notificationInfo: item,
              })
          })
        } else {
          return res.send(false);
        }
      })
    } else {
      return res.send(false);
    }
  })

  router.delete("/mevnah/notification/:objectID/:id", function(req, res, next) {
    if(req.isAuthenticated()) {
      let update = {
        $pull: {
          description: {
            _id: req.params.id,
          }
        }
      } 
      mongoose.model('notification').findByIdAndUpdate(req.params.objectID, update, {new: false},  (err, result) => {
        if (err) console.log('err', err);
        if (result) {
          return res.send(true);
        } else {
          return res.send(false);
        }
      })
    } else {
      return res.send(false);
    }
  })
};
