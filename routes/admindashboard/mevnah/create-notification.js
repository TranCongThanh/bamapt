var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/mevnah/create-notification/:id", function (req, res, next) {
    if (req.isAuthenticated()) {
      let query = {
        refMevnah: req.params.id
      }
      mongoose.model('notification').findOne(query, (err, result) => {
        if (err) throw err;
        // If this object hasn't created notification, create new
        // If did, push new to description
        if (!result) {
          mongoose.model('mevnah').findById(req.params.id, (err, info) => {
            if (err) throw err;
            if (info) {
              console.log(req.body);
              let {
                name,
                content,
                timeShow,
                timeEnd,
                background
              } = req.body;
              background = JSON.parse(background);
              let insert = {
                type: 'mevnah',
                refMevnah: info._id,
                districtsID: info.districtsID,
                wardsID: info.wardsID,
                description: [{
                  name: name,
                  content: content,
                  timeShow: timeShow,
                  timeEnd: timeEnd,
                  background: background
                }]
              }
              mongoose.model('notification').create(insert, (err, notifi) => {
                if (err) throw err;
                if (notifi)
                  return res.send(notifi)
                else return res.send(false);
              })
            }
          })
        } else {
          let {
            name,
            content,
            timeShow,
            timeEnd,
            background
          } = req.body;
          background = JSON.parse(background);
          let update = {          
            $push: {
              description: {
                name: name,
                content: content,
                timeShow: timeShow,
                timeEnd: timeEnd,
                background: background
              }
            }
          }
          mongoose.model('notification').findByIdAndUpdate(result._id, update, { new: false }, (err, notifi) => {
            if (err) console.log('err', err)
            console.log('notifi', notifi);
            if (notifi) {
              return res.send(notifi);
            }
            else return res.send(false);
          })
        }
      })
    } else {
      return res.send(false);
    }
  });

};
