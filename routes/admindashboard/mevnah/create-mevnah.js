var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");
var qrCode = require("../../../API/createQRCode");
var config = require("../../../config/index");
var list = require('../../../API/sendDistrictsAndWardsList');
var router = express.Router();

module.exports = router => {

  router.get("/mevnah/create-mevnah", function (req, res, next) {
    if (req.isAuthenticated()) {

      list.SendMeList(req.user.level, req.user.districtsID, req.user.wardsID, list => {
        if (req.user.level == 1) {
          res.render("admindashboard/mevnah/create-mevnah", {
            title: "Thêm thông tin Ba Má PT | Admin",
            parent_directory: "BA MÁ PHONG TRÀO",
            directory: "Thêm thông tin Ba Má PT",
            districtList: list,
            wardList: [],
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
        else if (req.user.level == 2) {
          let identifyDistrict = []
          identifyDistrict.push(list[0])
          res.render("admindashboard/mevnah/create-mevnah", {
            title: "Thêm thông tin Ba Má PT | Admin",
            parent_directory: "BA MÁ PHONG TRÀO",
            directory: "Thêm thông tin Ba Má PT",
            districtList: identifyDistrict,
            wardList: list[1],
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
        else if (req.user.level == 3) {
          let identifyDistrict = []
          let identifyWard = []
          identifyDistrict.push(list.districtsID)
          identifyWard.push(list)
          res.render("admindashboard/mevnah/create-mevnah", {
            title: "Thêm thông tin Ba Má PT | Admin",
            parent_directory: "BA MÁ PHONG TRÀO",
            directory: "Thêm thông tin Ba Má PT",
            districtList: identifyDistrict,
            wardList: identifyWard,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
      })

    } else {
      res.redirect("/admin/login");
    }
  });

  router.post('/mevnah/create-mevnah', (req, res, next) => {
    if (req.isAuthenticated()) {

      console.log('req.body', req.body);
      var currentTime = new Date();
      var {
        name,
        nickName,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        healthStatus,
        dayOfDeath,
        monthOfDeath,
        yearOfDeath,
        locationTomb,
        districtsID,
        wardsID,
        address,
        location,
        locatTomb,
        homeTown,
        martyrs,
        background,
        motherImageLink,
        victory,
        story,
        stages,
        prize,
        someOfOtherImage,
        nameContact,
        numberPhoneContact,
        emailContact,
        addressContact,
      } = req.body;

      background = JSON.parse(background);
      motherImageLink = JSON.parse(motherImageLink);
      story = JSON.parse(story);
      stages = JSON.parse(stages);
      prize = JSON.parse(prize);
      someOfOtherImage = JSON.parse(someOfOtherImage);
      martyrs = JSON.parse(martyrs);
      victory = JSON.parse(victory);
      location = JSON.parse(location);
      locatTomb = JSON.parse(locatTomb);
      var insert = {
        name: name,
        nickName: nickName,
        slug: `${slugify(name.toLowerCase())}-${currentTime.getTime()}`,
        address: {
          address: address,
          lat: location.lat,
          long: location.lng
        },
        postedBy: req.user._id,
        districtsID: districtsID,
        wardsID: wardsID,
        page: {
          homeTown: homeTown,
          birthday: {
            date: dayOfBirth,
            month: monthOfBirth,
            year: yearOfBirth,
          },
          healthStatus: healthStatus,
          dateOfDeath: {
            date: dayOfDeath,
            month: monthOfDeath,
            year: yearOfDeath
          },
          locationTomb: {
            address: locationTomb,
            lat: locatTomb.lat,
            long: locatTomb.lng
          },
          background: background,
          motherImageLink: motherImageLink,
          story: story,
          stages: stages,
          victory: victory,
          prize: prize,
          someOfOtherImage: someOfOtherImage,
        },
        martyrs: martyrs,
        contact: {
          numberPhoneContact: numberPhoneContact,
          emailContact: emailContact,
          addressContact: addressContact,
          nameContact: nameContact,
        }
      }
      console.log('insert', insert);
      mongoose.model('mevnah').create(insert, (err, mevnahInfo) => {
        if (err) {
          console.log('err', err);
          return res.send(false)
        }
        if (mevnahInfo) {
          // res.send(true);
          console.log('mevnahInfo', mevnahInfo)
          mongoose.model('mevnah').findById(mevnahInfo._id)
            .populate("districtsID")
            .populate("wardsID")
            .exec((err, info) => {
              if (err) console.log('err', err)
              if (info) {
                let districtSlug = info.districtsID ? info.districtsID.districtSlug : null;
                let wardSlug = info.wardsID ? info.wardsID.wardSlug : null;
                let link = `${config.SERVERURL}/mevnah/${districtSlug}/${wardSlug}/${info.slug}`;
                console.log('link', link)
                qrCode.CreateAndSave('mevnah', link, info._id, qrLink => {
                  let update = {
                    imageQRLink: qrLink,
                  }
                  mongoose.model('mevnah').findByIdAndUpdate(info._id, update, { new: false }, (err, finalResult) => {
                    if (err) console.log('err', err)
                  })
                });
              }
            })
        }
        return res.send(false);
      })
    } else {
      return res.send(false);
    }
  })
};
