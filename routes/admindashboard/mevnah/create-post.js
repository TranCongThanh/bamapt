var express = require("express");
var mongoose = require("mongoose");
var list = require('../../../API/sendDistrictsAndWardsList');
var router = express.Router();

module.exports = router => {
  router.get("/mevnah/:id/post", function (req, res, next) {

    if (req.isAuthenticated()) {
      mongoose.model('mevnah').findById(req.params.id, (err, result) => {
        if (err) throw err;
        if (result) {
          res.render("admindashboard/mevnah/create-posts", {
            title: "Chỉnh sửa thông tin BMPT| Admin",
            parent_directory: "BA MÁ PHONG TRÀO",
            directory: "Chỉnh sửa thông tin BMPT",
            id: req.params.id,
            content: result.posts.content,
            titlePosts: result.posts.title,
            motherName: result.name,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
      })
    } else {
      res.redirect('/admin/login');
    }
  });

  router.put("/mevnah/:id/post", function (req, res, next) {
    if (req.isAuthenticated()) {
      let update = {
        posts: {
          title: req.body.title ? req.body.title : null,
          content: req.body.content ? req.body.content : null,
          postedBy: req.user._id,
        }
      }
      mongoose.model('mevnah').findByIdAndUpdate(req.params.id, update, {new: false}, (err, result) => {
        if (err) return res.send(false);
        if (result) {
          return res.send(result);
        }
      })
    } else {
      return res.send(false);
    }
  });
};
