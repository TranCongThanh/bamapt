var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/mevnah/showAllNotification", function(req, res, next) {
    if (req.isAuthenticated()) {
      var query = {}
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
        }
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
        }
      }
      mongoose.model('mevnah').find(query)
      .populate('districtsID')
      .populate('wardsID')
      .exec((err, mevnahList) => {
        if (err) throw err;
        if (mevnahList) {
          res.render("admindashboard/mevnah/showAllNotification", {
            title: "Quản lý thông báo | Admin",
            parent_directory: "Tất cả thông báo",
            directory: "Tất cả thông báo",
            mevnahList,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
      })
    } else {
      res.redirect('/admin/login');
    }
  });

  router.get("/mevnah/notification/:id/show", function(req, res, next) {
    if (req.isAuthenticated()) {
      console.log('id', req.params.id);
      var query = {
        refMevnah: req.params.id,
      }
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
          refMevnah: req.params.id,
        }
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
          refMevnah: req.params.id,
        }
      }
      mongoose.model('notification').findOne(query)
      .populate('districtsID')
      .populate('wardsID')
      .exec((err, notificationInfo) => {
        if (err) return res.send(false);
        if (notificationInfo ? notificationInfo.description[0] : false) {
          console.log('abcd')
          return res.send({
            notificationInfo,
            haveNotification: true
          })
        } else {
          console.log('abc')
          return res.send({
            haveNotification: false,
          });
        }
      })
    } else {
      return res.send(false);
    }
  });

};
