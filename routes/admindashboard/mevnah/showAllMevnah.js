var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/mevnah/listAllMevnah", function(req, res, next) {
    if (req.isAuthenticated()) {
      var query = {};
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
        };
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID
        };
      }
      mongoose
        .model("mevnah")
        .find(query)
        .populate("wardsID")
        .populate("districtsID")
        .populate("qrCodeID")
        .populate("postedBy")
        .exec((err, mevnahList) => {
          if (err) throw err;
          if (mevnahList) {
            res.render("admindashboard/mevnah", {
              title: "BA MÁ PHONG TRÀO | Admin",
              parent_directory: "BA MÁ PHONG TRÀO",
              directory: "BA MÁ PHONG TRÀO",
              mevnahList: mevnahList,
              level: req.user.level,
              isDeveloper: req.user.isDeveloper,
            });
          }
        });
    } else {
      res.redirect('/admin/login');
    }
  });

};
