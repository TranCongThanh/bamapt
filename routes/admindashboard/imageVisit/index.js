module.exports = router => {
  require('./get')(router);
  require('./create')(router);
  require('./showHomepage')(router);
  require('./delete')(router);
}