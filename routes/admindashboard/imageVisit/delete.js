var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");

module.exports = router => {
  router.delete('/hinh-anh-tham-me/:id', async (req, res, next) => {
    if (req.isAuthenticated()) {
      await mongoose.model('imageVisit').findOneAndDelete({ _id: req.params.id })
      return res.send(true)
    } else {
      res.redirect("/admin/login");
    }
  })
}