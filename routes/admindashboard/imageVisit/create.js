var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");
var uploadFile = require('../../../API/uploadFile');


module.exports = router => {
  var uploadImage = uploadFile.StoreFile('images');
  router.post('/hinh-anh-tham-me', async (req, res, next) => {
    if (req.isAuthenticated()) {
      let imageUrl = JSON.parse(req.body.imgUrl);

      let create = {
        title: req.body.title,
        content: req.body.content,
        imageLink: imageUrl[0].imageLink,
        postedBy: req.user._id,
        slug: slugify(`{${req.body.title.toLowerCase()}-${new Date().getDate()}}`)
      }

      await mongoose.model('imageVisit').create(create);
      return res.redirect('/admin/hinh-anh-tham-me')
      
    } else {
      res.redirect("/admin/login");
    }
    
  })
}