var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");

module.exports = router => {
  router.put('/hinh-anh-tham-me/:id/show/:status', async (req, res, next) => {
    if (req.isAuthenticated()) {
      let key = false;
      if (req.params.status == "open") {
        key = true;
      }
      let update = {
        showHomepage: key
      }
      let option = { new: true }
      let vistor = await mongoose.model('imageVisit').findOneAndUpdate({ _id: req.params.id }, update, option);
      
      return res.send({ vistor })
    } else {
      res.redirect("/admin/login");
    }
  })
}