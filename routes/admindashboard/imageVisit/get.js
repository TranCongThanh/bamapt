var express = require("express");
var slugify = require('slugify');
var mongoose = require("mongoose");

module.exports = router => {
  router.get('/hinh-anh-tham-me', async (req, res, next) => {
    if (req.isAuthenticated()) {
      let vistor = await mongoose.model('imageVisit').find().populate('refMevnah');
      console.log(vistor)
      return res.render('admindashboard/imageVisit', {
        title: "Hình ảnh thăm ba má | Admin",
        parent_directory: "Hình ảnh",
        directory: "Hình ảnh thăm ba má",
        level: req.user.level,
        isDeveloper: req.user.isDeveloper,
        vistor: vistor
      })
    } else {
      res.redirect("/admin/login");
    }
  })
}