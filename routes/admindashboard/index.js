var express = require('express');
var router = express.Router();
require('./imageVisit')(router);
// Include Dashboard
require('./dashboard/index')(router);

// Include Users
require('./users/index')(router);

// Include Diachido
// require('./diachido/index')(router);

// Include Reply
require('./reply/index')(router);

// Include Report
require('./report/index')(router);

// Include qrcodes
require('./qrcodes/index')(router);

// Include image
require('./image/index')(router);

// Include system config
require('./system/index')(router);

// Include mevnah
require('./mevnah/index')(router);



module.exports = router;
