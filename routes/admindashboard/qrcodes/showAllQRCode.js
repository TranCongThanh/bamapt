var express = require('express');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var report = require('../../../API/googleAnalytics');
var config = require('../../../config/index');

var router = express.Router();

module.exports = router => {

  router.get('/qrcodes', (req, res, next) => {
    
    if (req.isAuthenticated()){
      // Query all qr codes
      var query = {}
      
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
        }
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
        }
      }
      let time = new Date();
      let startOfMonth = `${time.getFullYear()}-${("0" + (time.getMonth() + 1)).slice(-2)}-01`;
      mongoose.model('mevnah').find(query)
        .populate('qrCodeID')
        .populate('districtsID')
        .populate('wardsID')
        .exec((err, meVNAHList) => {
          if (err) throw err;
          res.render('admindashboard/qrcodes', { 
            title: "Mã QR | Admin", 
            parent_directory: "Địa chỉ đỏ", 
            directory: "Tất cả mã QR",
            diachidoList: [],
            meVNAHList: meVNAHList,
            startOfMonth: startOfMonth,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          })
        })
      // mongoose.model('diachido').find(query)
      //   .populate('qrCodeID')
      //   .populate('wardsID')
      //   .exec((err, diachidoList) => {
      //     if (err) throw err;
      // })

    } else res.redirect('/admin/login');
  });
}