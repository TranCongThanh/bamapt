var express = require('express');
var mongoose = require('mongoose');
var ua = require('universal-analytics');
var config = require('../../../config/index');
var report = require('../../../API/googleAnalytics');
var prettyjson = require('prettyjson');
var moment = require('moment');
var router = express.Router();

const visitor = ua(config.GoogleAnalyticsID);

module.exports = router => {
  router.get('/connectChart/:type', (req, res, next) => {
    if (req.isAuthenticated) {
      var time = new Date();
      var startDate;
      if (req.params.type == 'date') {
        startDate = `${time.getFullYear()}-${("0" + (time.getMonth() + 1)).slice(-2)}-01`;
      } else {
        startDate = `${time.getFullYear()}-01-01`
      }
      var query = {};
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID
        }
      };
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          _id: req.user.wardsID
        }
      }
      mongoose.model('wards').find(query)
        .populate('districtsID')
        .exec((err, result) => {
          if (err) return res.send(false);
          if (result) {
            var link = '/mevnah';
            if (req.user.level == 2) {
              link = `/mevnah/${result.districtsID ? result.districtsID.districtName : ''}`
            }
            if (req.user.level == 3) {
              link = `/mevnah/${result.districtsID ? result.districtsID.districtName : ''}/${result.wardName}`
            }
            report.numberOfConnectDashboard(startDate, 'today', link, req.params.type, async (numberBack) => {
              var pretty_json_options = {
                noColor: false
              };
              let data = [];
              let lables = ['Số lượt xem'];
              await numberBack.reports[0].data.rows.map((item, index) => {
                // lables.push(item.dimensions[0])
                data.push({
                  period: req.params.type == 'date' ? moment(item.dimensions[0]).format('YYYY-MM-DD') : moment(`${time.getFullYear()}-${item.dimensions[0]}`).format('YYYY-MM'),
                  'Số lượt xem': parseInt(item.metrics[0].values[0] ? item.metrics[0].values[0] : 0),
                  // 'Thời gian đọc trung bình': parseInt(item.metrics[0].values[1] ? item.metrics[0].values[1] : 0),
                  
                })
              })
              console.log(prettyjson.render(numberBack, pretty_json_options));
              return res.send({
                data,
                lables
              });
            })
          }
        })
    } else {
      res.send({
        death: 0,
        good: 0,
        weak: 0,
      });
    }
  })
};
