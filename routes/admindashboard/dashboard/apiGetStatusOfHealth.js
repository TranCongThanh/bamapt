var express = require('express');
var mongoose = require('mongoose');
var ua = require('universal-analytics');
var config = require('../../../config/index');
var report = require('../../../API/googleAnalytics');
var router = express.Router();

const visitor = ua(config.GoogleAnalyticsID);

module.exports = router => {
  router.get('/statusOfHealth', (req, res, next) => {
    if (req.isAuthenticated()) {
      var queryDeath = {
        'page.healthStatus': 'death'
      }
      var queryGood = {
        'page.healthStatus': 'good',
      }
      var queryWeak = {
        'page.healthStatus': 'weak',
      }
      if (req.user.level == 2) {
        queryDeath = {
          districtsID: req.user.districtsID,
          'page.healthStatus': 'death',
        },
        queryGood = {
          districtsID: req.user.districtsID,
          'page.healthStatus': 'good',
        }
        queryWeak = {
          districtsID: req.user.districtsID,
          'page.healthStatus': 'weak',
        }
      }
      if (req.user.level == 3) {
        queryDeath = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
          'page.healthStatus': 'death',
        },
        queryGood = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
          'page.healthStatus': 'good',
        }
        queryWeak = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
          'page.healthStatus': 'weak',
        }
      }
      console.log('queryDeath', queryDeath)
      mongoose.model('mevnah').count(queryWeak, (err, weak) => {
        if (err) throw err;
        mongoose.model('mevnah').count(queryDeath, (err, death) => {
          if (err) throw err;
          mongoose.model('mevnah').count(queryGood, (err, good) => {
            console.log('weak', weak);
            console.log('good', good);
            console.log('death', death);
            res.send({
              death: death,
              good: good,
              weak: weak,
            })
          })
        })
      })
    } else {
      res.send({
        death: 0,
        good: 0,
        weak: 0,
      });
    }
  })
};
