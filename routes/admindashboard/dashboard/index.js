var express = require('express');
var mongoose = require('mongoose');
var ua = require('universal-analytics');
var config = require('../../../config/index');
var report = require('../../../API/googleAnalytics');
var router = express.Router();

const visitor = ua(config.GoogleAnalyticsID);

module.exports = router => {
  require('./apiGetStatusOfHealth')(router);
  require('./connectChar')(router);
  router.get('/', async (req, res, next) => {
    console.log(req)
    if (req.isAuthenticated()) {
      // Nếu người dùng cấp quản lý sẽ xem được các hoạt động đang diễn ra trên hệ thống và các phản hồi mới
      // Nếu người dùng cấp dưới sẽ xem được
      // if (req.user.level == 1) {
      //   var querySystemLog = { type: 'systemlog' }
      //   mongoose.model('logs').find(querySystemLog)
      //   .sort({ createdTime: 1 })
      //   .limit(5)
      //   .exec((err, result) => {
      //     if (err) throw err;
      //     var queryNewReply = {
      //       type: 'rely'
      //     }
      //   })
      // } else {
        
      // }
      let time = new Date();
      var query = {}
      if (req.user.level == 2) {
        query = {
          districtsID: req.user.districtsID,
        }
      }
      if (req.user.level == 3) {
        query = {
          districtsID: req.user.districtsID,
          wardsID: req.user.wardsID,
        }
      }
      report.mainPage(`${time.getFullYear()}-${("0" + (time.getMonth() + 1)).slice(-2)}-01`, 'today', reportRespond => {
        mongoose.model('mevnah').count(query, (err, totalMevnah) => {
          if (err) throw err;
          mongoose.model('replyFromUsers').count(query, (err, totalReply) => {
            if (err) throw err;
            res.render('admindashboard/dashboard/index', {
              title: "Quản lý | Admin", 
              parent_directory: "Dashboard", 
              directory: "Quản lý",
              reportRespond: reportRespond.reports[0].data,
              totalMevnah: totalMevnah,
              totalReply: totalReply,
              level: req.user.level,
              isDeveloper: req.user.isDeveloper,
            });
          })
        })
      });
    } else res.redirect('/admin/login');
  });

  

  router.get('/analytic', function(req, res, next) {
    res.render('admindashboard/dashboard/analytic', { title: "Phân tích | Admin", parent_directory: "Dashboard", directory: "Phân tích"});
  });
};
