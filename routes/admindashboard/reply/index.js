var express = require('express');
var mongoose = require('mongoose');
var sendEmail = require('../../../API/sendEmail');
var ejs = require('ejs');
var router = express.Router();

receivedInfo1 = (replyInfo, user, body) => {
  ejs.renderFile(__dirname + '/emailTemplate/replyUsers.ejs', {
    name: replyInfo.name,
    userName: user.name,
    email: user.email,
    content: body.replyUser,
  }, (err, html) => {
    if (err) throw err;
    sendEmail.Send(replyInfo.email, `[bamaphongtraohssv.vn] - Phản hồi nội dung thắc mắc từ bạn ${replyInfo.name}`, html)
    // createLog.newReply('reply', body.name, body.content)
  })
}

module.exports = router => {
  router.get('/reply', function(req, res, next){
    if (req.isAuthenticated()) {
      var query = {
        isReply: false,
        districtsID: req.user.districtsID ? req.user.districtsID : null,
        wardsID: req.user.wardsID ? req.user.wardsID : null,
      }
      if (req.user.level == 1) {
        query = {
          isReply: false,
        };
      }
      if (req.user.level == 2) {
        query = {
          isReply: false,
          districtsID: req.user.districtsID ? req.user.districtsID : null,
        }
      }
      mongoose.model('replyFromUsers').find(query)
      .limit(50)
      .sort({
        createdDate: 1
      })
      .populate({
        path: 'refDiachido',
        populate: {
          path: 'wardsID',
          populate: {
            path: 'districtsID',
          }
        },
      })
      .populate({
        path: 'refMevnah',
        populate: {
          path: 'wardsID',
          populate: {
            path: 'districtsID',
          }
        },
      })
      .exec((err, replyListFalse) => {
        if (err) throw err;
        query.isReply = true;
        mongoose.model('replyFromUsers').find(query)
        .limit(50)
        .sort({
          createdDate: 1
        })
        .populate({
          path: 'refDiachido',
          populate: {
            path: 'wardsID',
            populate: {
              path: 'districtsID',
            }
          },
        })
        .populate({
          path: 'refMevnah',
          populate: {
            path: 'wardsID',
            populate: {
              path: 'districtsID',
            }
          },
        })
        .exec((err, replyListTrue) => {
          console.log(replyListFalse)
          if (err) throw err;
          res.render('admindashboard/reply', { 
            title: "Phản hồi | Admin", 
            parent_directory: "Báo cáo", 
            directory: "Phản hồi",
            replyListTrue: replyListTrue,
            replyListFalse: replyListFalse,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          })
        })
      })
    
    } else {
      res.redirect('/admin/login');
    }
  })


  router.post('/reply/:id', (req, res, next) => {
    if (req.isAuthenticated()) {
      let update = {
        isReply: true,
      }
      mongoose.model('replyFromUsers').findByIdAndUpdate(req.params.id, update, {new: false}, (err, replyInfo) => {
        if (err) throw err;
        if (replyInfo) {
          console.log(replyInfo)
          receivedInfo1(replyInfo, req.user, req.body);
          res.redirect('/admin/reply');
        }
      })
    } else {
      res.redirect('/admin/login');
    }
  })
};
