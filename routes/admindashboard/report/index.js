var express = require("express");
var ua = require('universal-analytics');
var config = require('../../../config/index');
var router = express.Router();

const visitor = ua(config.GoogleAnalyticsID);

module.exports = router => {
  router.get("/report", function(req, res, next) {
    // if (req.isAuthenticated()) {
      console.log('visitor', visitor)
      res.render("admindashboard/report", {
        title: "Báo cáo | Admin",
        parent_directory: "Báo cáo",
        directory: "Báo cáo - Thống kê",
        level: 1,
        isDeveloper: true,
      });
    // } else {
      // res.redirect('/admin/login');
    // }
  });
};
