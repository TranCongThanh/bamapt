var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/removeWard/:wardID", async (req, res, next) =>  {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let remove = {
        _id: req.params.wardID,
      };
      mongoose.model('wards').deleteOne(remove, (err, result) => {
        if (err) throw err;
      })
      res.redirect('/admin/system/listAllDistricts')
    } else {
      res.redirect('/admin');
    }
  });
};