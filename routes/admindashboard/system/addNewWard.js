var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/addNewWard/:districtID", async (req, res, next) =>  {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let wardList = req.body.wardName.split(';');
      await wardList.map(item => {
        let insert = {
          districtsID: req.params.districtID,
          wardName: item.trim(),
          wardSlug: slugify(item.trim().toLowerCase())
        };
        mongoose.model('wards').create(insert, (err, result) => {
          if (err) throw err;
        })
      })
      res.redirect('/admin/system/listAllDistricts')
    } else {
      res.redirect('/admin');
    }
  });
};