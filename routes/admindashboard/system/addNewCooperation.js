var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/addNewCooperation", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let insert = {
        name: req.body.name,
        imageLink: JSON.parse(req.body.imageLink)[0].link,
        contactName: req.body.contactName,
        contactPhone: req.body.contactPhone,
        contactEmail: req.body.contactEmail,
        link: req.body.link,
      };
      mongoose.model('cooperation').create(insert, (err, result) => {
        if (err) throw err;
          res.send(true)
      })
    } else {
      res.redirect('/admin');
    }
  });
};