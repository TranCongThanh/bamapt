var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/system/listWards/:districtSlug", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let query = {
        districtSlug: req.params.districtSlug,
      }
      mongoose.model('districts').findOne(query, (err, districtInfo) => {
        if (err) throw err;
        if (districtInfo) {
          let query = {
            districtsID: districtInfo._id
          }
          mongoose.model('wards').find(query)
            .sort({
              wardSlug: 1
            })
            .populate('districtsID')
            .exec((err, wardList) => {
              if (err) throw err;
              console.log('wardList', wardList)
              res.render("admindashboard/system/listWards", {
                title: "Báo cáo | Admin",
                parent_directory: "Quản lý hệ thống",
                directory: "Quản lý các địa phương",
                wardList: wardList,
                districtsID: districtInfo._id,
                level: req.user.level,
                isDeveloper: req.user.isDeveloper,
              });
            })
        }
      })
    } else {
      res.redirect('/admin');
    }
  });
};
