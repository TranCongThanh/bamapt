var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/system/listAllDistricts", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      mongoose.model('districts').find({})
      .sort({
        districtSlug: 1
      })
      .exec((err, districtList) => {
        if (err) throw err;
        if (districtList) {
          res.render("admindashboard/system/listAllDistricts", {
            title: "Quản lý Quận huyện | Admin",
            parent_directory: "Quản lý hệ thống",
            directory: "Quản lý Master data",
            districtList: districtList,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          });
        }
      })
    } else {
      res.redirect('/admin');
    }
  });
};
