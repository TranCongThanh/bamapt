var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/removeUsers/:user_id", async (req, res, next) =>  {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let remove = {
        _id: req.params.user_id,
      };
      let update = {
        isBanned: true,
      }
      let option = { new: false }
      mongoose.model('users').findOneAndUpdate(remove, update, option, (err, result) => {
        if (err) throw err;
        if (result) {
          res.redirect('/admin/system/listAllUsers')
        }
      })
    } else {
      res.redirect('/admin');
    }
  });
};