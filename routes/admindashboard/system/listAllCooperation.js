var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/system/listAllCooperation", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      mongoose.model('cooperation').find({}, (err, result) => {
        if (err) throw err;
        if (result)
          res.render('admindashboard/system/listAllCooperation', {
            title: "Quản lý đơn vị hợp tác | Admin",
            parent_directory: "Quản lý hệ thống",
            directory: "Quản lý Master data",
            cooperationList: result,
            level: req.user.level,
            isDeveloper: req.user.isDeveloper,
          })
      })
    } else {
      res.redirect('/admin');
    }
  });
};
