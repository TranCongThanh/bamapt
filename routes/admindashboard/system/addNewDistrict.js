var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/addNewDistrict", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let insert = {
        districtName: req.body.districtName,
        districtSlug: slugify(req.body.districtName.toLowerCase()),
        imageLink: JSON.parse(req.body.imgUrl)[0].link
      };
      mongoose.model('districts').create(insert, (err, result) => {
        if (err) {
          console.log('err', err)
          throw err;
        }
        res.send(true);        
          // res.redirect('/admin/system/listAllDistricts')
      })
    } else {
      res.send(false);
      // res.redirect('/admin');
    }
  });
};