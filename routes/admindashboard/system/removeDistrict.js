var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/removeDistrict/:districtID", async (req, res, next) =>  {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let remove = {
        districtsID: req.params.districtID,
      };
      mongoose.model('wards').deleteMany(remove, (err, result) => {
        if (err) throw err;
        let remove = {
          _id: req.params.districtID
        }
        mongoose.model('districts').deleteOne(remove, (err, result) => {
          if (err) throw err;
        })
      })
      res.redirect('/admin/system/listAllDistricts')
    } else {
      res.redirect('/admin');
    }
  });
};