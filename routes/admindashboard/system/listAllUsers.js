var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.get("/system/listAllUsers", function(req, res, next) {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      let query = { $or: [{
        level: 1,
        level: 2,
      }] }
      mongoose.model('users').find(query)
        .sort({
          level: 1
        })
        .populate('districtsID')
        .exec((err, usersList) => {
          if (err) throw err;
          if (usersList) {
            res.render("admindashboard/system/listAllUsers", {
              title: "Báo cáo | Admin",
              parent_directory: "Quản lý hệ thống",
              directory: "Quản lý Master data",
              usersList: usersList,
              level: req.user.level,
              isDeveloper: req.user.isDeveloper,
            });
          }
        })
    } else {
      res.redirect('/admin');
    }
  });
};
