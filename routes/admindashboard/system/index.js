var express = require('express');
var router = express.Router();


module.exports = router => {
  require('./listAllDistricts')(router);
  require('./listAllUsers')(router);
  require('./listAllCooperation')(router);
  require('./listWards')(router);
  require('./addNewDistrict')(router);
  require('./addNewCooperation')(router);
  require('./addNewWard')(router);
  require('./removeWard')(router);
  require('./removeCooperation')(router);
  require('./removeDistrict')(router);
  require('./removeUsers')(router);
};
