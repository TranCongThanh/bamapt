var slugify = require('slugify')
var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();

module.exports = router => {
  router.post("/system/removeCooperation/:id", async (req, res, next) =>  {
    if (req.isAuthenticated() && req.user.isDeveloper == true) {
      mongoose.model('cooperation').findByIdAndRemove(req.params.id, (err, result) => {
        if (err) throw err;
        if (result)
          res.redirect('/admin/system/listAllCooperation');
      })
    } else {
      res.redirect('/admin');
    }
  });
};