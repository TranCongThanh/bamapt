// This file just use method POST
var express = require('express');
var passport = require('passport');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var randomString = require('randomstring');
var ejs = require('ejs');
var sendEmail = require('../API/sendEmail');

var router = express.Router();


router.get('/forget-password/:userID/:token', (req, res, next) => {
  let query = {
    user: req.params.userID,
    token: req.params.token,
    isActive: false,
  };
  let update = { acceptToken: true };
  let option = { new: false };
  mongoose.model('forgetPassword').findOneAndUpdate(query, update, option, (err, result) => {
    if (err) throw err;
    if (result) {
      res.render('admindashboard/users/new-password', { 
        title: "Đổi mật khẩu | Admin",
        mess: '',
        token: req.params.token,
      })
    } else {
      // If this token has been actived
      res.redirect('/admin/login');
    }
  })
})

module.exports = router;