var express = require('express');
var router = express.Router();

require('./homepage/index')(router);
require('./notification/index')(router);

module.exports = router;
