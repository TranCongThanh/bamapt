var mongoose = require('mongoose');
var config = require('../../../config/index');

module.exports = router => {
  router.get('/checknotification/:id', function(req, res, next) {
    let currentDate = new Date();
    let query = {
      type: 'mevnah',
      refMevnah: req.params.id,
      description: {
        $all: [{
          timeShow: {
            "$lte": currentDate,
          },
        }, {
          timeEnd: {
            "$gte": currentDate, 
          },
        }]
      }
    }
    mongoose.model('notification').find(query)
    .sort({
      createdDate: -1,
    })
    .exec((err, result) => {
      if (err) return res.send(false);
      if (result) {
        return res.send(result.description)
      } else {
        return res.send(false);
      }
    })
  });
}
