var express = require('express');
var mongoose = require('mongoose');
var fs = require('fs');
var router = express.Router();
var compress_images = require('compress-images');
var fs = require('fs');
var report = require('../API/googleAnalytics');
var uploadImage = require('../API/uploadFile');
/* GET users listing. */
router.get('/', function(req, res, next) {
  // let query = {
  //   districtsID: '5b44c6501b49e221b0c448bf'
  // }
  // mongoose.model('wards').find(query)
  // .populate('districtsID')
  // .sort({
  //   wardSlug: -1
  // })
  // .limit(1)
  // .exec((err, result) => {
  //   if (err) throw err;
  //   console.log(result)
  //   res.render('test');
  // })
  res.render('test')
});


var upload = uploadImage.StoreFile('images');
router.post('/image', upload.single('image'), (req, res, next) => {
  const fileId = req.file.filename.split('.')[0];
  // Remove public in destination and add filename in the link
  console.log('abc');
  req.file.link = req.file.destination.substring(6, req.file.destination.length) + '/' + req.file.filename;
  if (req.isAuthenticated()) {
    uploadImage.updateUser(req.user._id, fileId, next => {
      if (next)
        return res.send(req.file)
      return res.send(false);
    });
  } else {
    uploadImage.removeFile(fileId);
    return res.send(false);
  }
})

var upload = uploadImage.StoreFile('abc','xyz','img');
router.post('/', upload.any(), (req, res, next) => {
  
  console.log('req.body', req.body);
  res.send(req.files[0].path)
  // return (req.files[0].path);
  
    // res.redirect('/test');
})

function handleReportingResults(response, cb) {
  if (!response.code) {
    // outputToPage('Query Success');
    var output = []
    for( var i = 0, report; report = response.reports[ i ]; ++i )
    {
      output.push('<h3>All Rows Of Data</h3>');
      if (report.data.rows && report.data.rows.length) {
        var table = ['<table>'];

        // Put headers in table.
        table.push('<tr><th>', report.columnHeader.dimensions.join('</th><th>'), '</th>');
        table.push('<th>Date range #</th>');

        for (var i=0, header; header = report.columnHeader.metricHeader.metricHeaderEntries[i]; ++i) {
          table.push('<th>', header.name, '</th>');
        }

        table.push('</tr>');

        // Put cells in table.
        for (var rowIndex=0, row; row = report.data.rows[rowIndex]; ++rowIndex) {
          for(var dateRangeIndex=0, dateRange; dateRange = row.metrics[dateRangeIndex]; ++dateRangeIndex) {
            // Put dimension values
            table.push('<tr><td>', row.dimensions.join('</td><td>'), '</td>');
            // Put metric values for the current date range
            table.push('<td>', dateRangeIndex, '</td><td>', dateRange.values.join('</td><td>'), '</td></tr>');
          }
        }
        table.push('</table>');

        cb(output);
      } else {
        output.push('<p>No rows found.</p>');
      }
    }
    outputToPage(output.join(''));

  } else {
    outputToPage('There was an error: ' + response.message);
  }
}

router.get('/test/test', (req, res, next) => {
  let time = new Date();
  report.mainPage(`${time.getFullYear()}-${("0" + (time.getMonth() + 1)).slice(-2)}-01`, 'today', reportRespond => {
    console.log('reportRespond', reportRespond)
    handleReportingResults(reportRespond, cb => {
      console.log('cb', cb)
      res.send(cb)
    })
    
  })
})
module.exports = router;
