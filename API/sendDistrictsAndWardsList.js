var mongoose = require('mongoose');

module.exports = {
 SendMeList: (level, districtsID, wardsID, callback) => {
  if (level == 1) {
    // If users level 1 send info of district first
    mongoose.model('districts').find({}, (err, districtsList) => {
      if (err) throw err;
      if (districtsList)
        callback(districtsList);
    })
  }

  else if (level == 2) {
    mongoose.model('districts').findById(districtsID, (err, districtInfo) => {
      if (err) throw err;
      mongoose.model('wards').find({districtsID: districtsID}, (err, wardsList) => {
        if (err) throw err;
        var list = [];
        list.push(districtInfo);
        list.push(wardsList);
        callback(list);
      })
    })
  } 
  
  else if (level == 3) {
    mongoose.model('wards').findById(wardsID).populate('districtsID')
    .exec((err, result) => {
      if (err) throw err;
      callback(result);
    })
  }

  else callback(false);
 }
}