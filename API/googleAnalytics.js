var prettyjson = require('prettyjson'); // Un-uglify JSON output
var {google} = require('googleapis');
var key = require('./bamaphongtraohssvvn-d4c2e8514fbe.json') // Downloaded JSON file
var config = require('../config/index');
var viewID = '179763078'; // Google Analytics view ID
var analytics = google.analyticsreporting('v4'); // Used for pulling report
var jwtClient = new google.auth.JWT(key.client_email, // For authenticating and permissions
                                    null,
                                    key.private_key,
                                    ['https://www.googleapis.com/auth/analytics.readonly'],
                                    null);
 
jwtClient.authorize(function (err, tokens) {
  if (err) {
    console.log('Reeeeejected');
    console.log(err);
    return;
  } else {
    console.log('Yup, we got authorized!');
  }
});

module.exports = {
  mainPage: (startDate, endDate, cb) => {
  // Set up what we data we want to pull from Google Analytics
  metrics_columns = [{
    expression: 'ga:users'
  }, {
    expression: 'ga:newUsers'
  }];
    
  date_filters = [{
    startDate,
    endDate,
    
  }];
  
  sort = [{
    fieldName: 'ga:users',
    sortOrder: "DESCENDING"
  }];
  
  var req = {
    reportRequests: [{
      viewId: viewID,
      dateRanges: date_filters,
      metrics: metrics_columns,
      orderBys: sort
    }],
  };
  
  // Pull report and output the data
  analytics.reports.batchGet({
      auth: jwtClient,
      resource: req
    },
    function (err, response) {
      if (err) {
        console.log('Failed to get Report');
        console.log(err);
        return cb(false);
      }
      return cb(response.data);
    
    });
  },

  qrCode: async (startDate, endDate, data, cb) => {
    var filters = []
    await data.map((item, index) => { 
      filters.push({
        "dimensionName": "ga:pagePath",
        "operator": "EXACT",
        "expressions": `/qrdirect/${item._id}`
      })
    })
    metrics_columns = [{
      expression: 'ga:users'
    }];

    dimensions_rows = [{
      name: 'ga:pagePath'
    }];

    dimensionFilterClauses = [{
      filters,
    }]
    
    date_filters = [{
      startDate,
      endDate,
    }];
    
    var req = {
      reportRequests: [{
        viewId: viewID,
        dateRanges: date_filters,
        metrics: metrics_columns,
        dimensions: dimensions_rows,
        dimensionFilterClauses: dimensionFilterClauses,
      }],
    };
    // Pull report and output the data
    analytics.reports.batchGet({
        auth: jwtClient,
        resource: req
      },
      function (err, response) {
        if (err) {
          console.log('Failed to get Report');
          console.log(err);
          return;
        }
        cb(response.data);
      }
    );
  },

  numberOfConnectDashboard: async(startDate, endDate, link, type, cb) => {
    console.log('link', link)
    // Set up what we data we want to pull from Google Analytics
    metrics_columns = [{
      expression: 'ga:pageviews'
    }, {
      expression: 'ga:avgTimeOnPage'
    }];
     
    dimensions_rows = [{
      name: `ga:${type == 'date' ? 'date' : 'month'}`
    }];

    dimensionFilterClauses = [{
      filters:  [
        { dimensionName: 'ga:pagePath',
        operator: 'BEGINS_WITH',
        expressions: `${link}` }
      ]
  }]

    date_filters = [{
      startDate,
      endDate,
    }];

    var req = {
      reportRequests: [{
        viewId: viewID,
        dateRanges: date_filters,
        dimensions: dimensions_rows,
        dimensionFilterClauses: dimensionFilterClauses,
        metrics: metrics_columns,
      }],
    };

    // Pull report and output the data
    analytics.reports.batchGet({
      auth: jwtClient,
      resource: req
    },
      function (err, response) {
        if (err) {
          console.log('Failed to get Report');
          console.log(err);
          return cb(false);
        }
        return cb(response.data);  
    });
  }
}