var multer = require('multer');
var mongoose = require('mongoose');
var path = require('path');
var compress_images = require('compress-images');
var fs = require('fs');

module.exports = {
  StoreFile: (type) => {
    var placeStore = `${type}/uploads/`;
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'public/' + placeStore)
      },
      filename: (req, file, cb) => {
        var fileName = file.originalname.split('.');
        let insert = {
          originalName: fileName[0],
          type: type,
        }
        mongoose.model('objectUpload').create(insert, (err, hanler) => {
          if (err) throw err;
          let name = `${hanler._id}.${fileName[1]}`;
          let link = '/' + placeStore + name;
          let update = { 
            link: link,
            fileName: name,
          }
          let option = { new: false }
          mongoose.model('objectUpload').findByIdAndUpdate(hanler._id, update, option, (err, hanlerLatest) => {
            if (err) throw err;

            cb(null, name);
          })
        })
      }
  });
    return multer({storage: storage});
  },
  updateUser: (user, fileId, cb) => {
    let update = {
      postedBy: user._id,
      districtsID: user.districtsID != '' ? user.districtsID : null,
      wardsID: user.wardsID != '' ? user.wardsID : null,
    }
    mongoose.model('objectUpload').findByIdAndUpdate(fileId, update, {new: false}, (err, exculte) => {
      if (err) throw err;
      if (exculte) {
        return cb(true);
      }
      return cb(false);
    })
  },

  compressImage: (fileId, endFile, type) => {
    var placeStore = `${type}/uploads/`;
    
    let INPUT_path_to_your_images = `public/${placeStore}**/${fileId}.{jpg,JPG,jpeg,JPEG,png,svg,gif}`;
    let OUTPUT_path = `public/${placeStore}temp/`;

    let fileTemp = `public/${placeStore}temp/${fileId}.${endFile}`;
    let fileSource = `public/${placeStore}${fileId}.${endFile}`;

    compress_images(INPUT_path_to_your_images, OUTPUT_path, {compress_force: false, statistic: true, autoupdate: true}, false,
      {jpg: {engine: 'mozjpeg', command: ['-quality', '60']}},
      {png: {engine: 'pngquant', command: ['--quality=20-50']}},
      {svg: {engine: 'svgo', command: '--multipass'}},
      {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}}, function(){
    });
    setTimeout(function() {
      fs.exists(fileTemp, (existsFileTemp) => {
        fs.exists(fileSource, (existsFileSource) => {
          if (existsFileSource && existsFileTemp) {
            fs.unlink(fileSource, err => { if(err) throw err })
            fs.copyFile(fileTemp, fileSource, (err) => {
              if (err) throw err;
              fs.unlink(fileTemp, err => { if(err) throw err })
            })
          }
        })
      })
    }, 10000)
  },

  removeFile: (fileId) => {
    mongoose.model('objectUpload').findByIdAndRemove(fileId, (err, exculte) => {
      if (err) throw err;
      if (exculte) {
        const linkImage = `public${exculte.link}`
        fs.exists(linkImage, (exists) => {
          if (exists) {
            fs.unlink(linkImage, err => { if(err) throw err });
            return true;
          } else return false;
        })
      }
    })
  }
}