var mongoose = require('mongoose');
var config = require('../config/index');
var express = require('express');
var qrCode = require('qrcode');

module.exports = {
  CreateAndSave: (type, directTo, id, cb) => {
    let insert = {
      type: type,
      directTo: directTo,
    }
    mongoose.model('qrCode').create(insert, (err, data) => {
      if (err) throw err;
      let imageLink = `/images/qrcodes/${type}/${data._id}.png`;
      let storePlace = `public/images/qrcodes/${type}/${data._id}.png`;
      let linkDirect = `/qrdirect/${data._id}`;
      qrCode.toFile(storePlace, linkDirect, (err) => {
        if (err) throw err;
        let update = { imageQRLink: imageLink };
        let option = { new: false };
        mongoose.model('qrCode').findByIdAndUpdate(data._id, update, option, (err, result) => {
          if (err) throw err;
          if (type == 'diachido') {
            let update = {
              qrCodeID: result._id,
            }
            mongoose.model('diachido').findByIdAndUpdate(id, update, {new: false}, (err, diachidoNew) => {
              if (err) throw err;
            })
          }
          else if (type == 'mevnah') {
            let update = {
              qrCodeID: result._id,
            }
            mongoose.model('mevnah').findByIdAndUpdate(id, update, {new: false}, (err, mevnahNew) => {
              if (err) throw err;
            })
          }
          cb(result.imageQRLink);
        })
      })
    })
  }
};
