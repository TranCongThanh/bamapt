var prettyjson = require('prettyjson'); // Un-uglify JSON output
var {google} = require('googleapis');
var key = require('./diachidoinfo-1450536186e2.json') // Downloaded JSON file
 
var viewID = '179042956'; // Google Analytics view ID
var analytics = google.analyticsreporting('v4'); // Used for pulling report
var jwtClient = new google.auth.JWT(key.client_email, // For authenticating and permissions
                                    null,
                                    key.private_key,
                                    ['https://www.googleapis.com/auth/analytics.readonly'],
                                    null);
 
jwtClient.authorize(function (err, tokens) {
  if (err) {
    console.log('Reeeeejected');
    console.log(err);
    return;
  } else {
    console.log('Yup, we got authorized!');
  }
});
 
// Set up what we data we want to pull from Google Analytics
metrics_columns = [{
  expression: 'ga:users'
}];
 
dimensions_rows = [{
  name: 'ga:sourceMedium'
}];
 
date_filters = [{
  startDate: 'yesterday',
  endDate: 'today',
}];
 
sort = [{
  fieldName: 'ga:users',
  sortOrder: "DESCENDING"
}];

dimensionFilterClauses = [{
  filters: [{
      "dimensionName": "ga:pagePath",
      "operator": "EXACT",
      "expressions": ["/"]
  }]
}]
 
var req = {
  reportRequests: [{
    viewId: viewID,
    dateRanges: date_filters,
    metrics: metrics_columns,
    dimensions: dimensions_rows,
    dimensionFilterClauses: dimensionFilterClauses,
    orderBys: sort
  }],
};
 
// Options for prettifying JSON output
var pretty_json_options = {
  noColor: false
};
 
// Pull report and output the data
analytics.reports.batchGet({
    auth: jwtClient,
    resource: req
  },
  function (err, response) {
    if (err) {
      console.log('Failed to get Report');
      console.log(err);
      return;
    }
    // response.data is where the good stuff is located
    console.log('Success - got something back from the Googlez');
    console.log(prettyjson.render(response.data, pretty_json_options));
  }
);