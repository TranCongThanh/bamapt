var mongoose = require("mongoose");

// Người dùng được phân theo cấp bậc người cấp 1 được quản lý cấp 2, và tương tự

var users = new mongoose.Schema({
  username: {
    required: true,
    type: String,
    index: true,
    unique: true
  },
  password: {
    required: true,
    type: String
  },
  email: {
    required: true,
    type: String
  },
  name: {
    required: true,
    type: String
  },
  level: {
    required: true,
    type: Number
  },
  districtsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  },
  isDeveloper: {
    required: true,
    default: false,
    type: Boolean
  },
  isBanned: {
    required: true,
    default: false,
    type: Boolean,
  }
});

var forgetPassword = new mongoose.Schema({
  user: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  email: {
    required: true,
    type: String
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date()
  },
  token: {
    required: true,
    type: String
  },
  isActive: {
    required: true,
    type: Boolean,
    default: false
  },
  acceptToken: {
    required: true,
    type: Boolean,
    default: false
  }
});

var objectUpload = new mongoose.Schema({
  originalName: {
    required: true,
    type: String
  },
  fileName: {
    required: false,
    type: String,
  },
  link: {
    required: false,
    type: String
  },
  isDiachido: {
    required: false,
    type: Boolean,
  },
  refDiachido: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'diachido'
  },
  refMevnah: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'mevnah'
  },
  type: {
    required: true,
    type: String,
    default: "images"
  },
  timeUpload: {
    required: true,
    type: Date,
    default: new Date()
  },
  postedBy: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  districtsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  }
});

var districts = new mongoose.Schema({
  districtName: {
    required: true,
    type: String
  },
  districtSlug: {
    required: true,
    type: String
  },
  imageLink: {
    required: false,
    type: String
  },
  alt: {
    type: String,
  },
});

var wards = new mongoose.Schema({
  districtsID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts",
  },
  wardName: {
    required: true,
    type: String
  },
  wardSlug: {
    required: true,
    type: String
  }
})

var dataNumber = new mongoose.Schema({
  pageName: {
    required: true,
    type: String,
  },
  slug: {
    required: true,
    type: String,
  },
  data: [{
    dataName: {
      required: true,
      type: String,
    },
    dataNumber: {
      required: true,
      type: Number,
    }
  }]
});

var logs = new mongoose.Schema({
  logName: {
    required: true,
    type: String,
  },
  type: {
    required: true,
    type: String,
  },
  userLog: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  userCreate: {
    required: false,
    type: String,
  },
  level: {
    required: true,
    type: Number
  },
  content: {
    required: true,
    type: String,
  },
  createdTime: {
    required: true,
    type: Date,
    default: new Date(),
  },
  status: {
    required: false,
    type: Boolean,
    default: false,
  }
})

var diachido = new mongoose.Schema({
  name: {
    required: true,
    type: String
  },
  slug: {
    required: true,
    type: String
  },
  address: {
    required: true,
    type: String
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date()
  },
  postedBy: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },

  districtsID: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  },
  qrCodeID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "qrCode",
  },
  isBanned: {
    required: true,
    type: Boolean,
    default: false,
  },
  page: {
    description: [{
      title: {
        type: String,
      },
      content: {
        type: String,
      }
    }],
    stages: [{
      time: {
        type: String,
      },
      desc: {
        type: String,
      },
      imageLink: {
        type: String
      },
      alt: {
        type: String,
      },
    }],
    someOfOtherImage: [{
      title: {
        type: String,
      },
      desc: {
        type: String,
      },
      imageLink: {
        type: String
      },
      alt: {
        type: String,
      },
    }],
    background: [{
      imageLink: {
        type: String
      },
      alt: {
        type: String,
      },
    }]
  },
  contact: {
    numberPhoneContact: {
      type: String,
    },
    emailContact: {
      type: String,
    },
    addressContact: {
      type: String,
    },
    nameContact: {
      type: String,
    }
  }
});

var mevnah = new mongoose.Schema({
  name: {
    required: true,
    type: String
  },
  nickName: {
    required: true,
    type: String,
  },
  slug: {
    required: true,
    type: String
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date()
  },
  address: {
    address: {
      type: String,
    },
    lat: {
      type: String,
    },
    long: {
      type: String,
    }
  },
  postedBy: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  districtsID: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  },
  qrCodeID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "qrCode",
  },
  isBanned: {
    required: true,
    type: Boolean,
    default: false,
  },

  page: {
    homeTown: {
      type: String,
    },
    birthday: {
      date: {
        type: String,
      },
      month: {
        type: String,
      },
      year: {
        type: String,
      }
    },
    dateOfDeath: {
      date: {
        type: String,
      },
      month: {
        type: String,
      },
      year: {
        type: String,
      }
    },
    healthStatus: {
      type: String,
      enum: ['good', 'death', 'weak'],
    },
    locationTomb: {
      address: {
        type: String,
      },
      lat: {
        type: String,
      },
      long: {
        type: String,
      }
    },
    background: [{
      imageLink: {
        type: String,
      }
    }],
    motherImageLink: [{
      imageLink: {
        type: String,
      },
      alt: {
        type: String,
      },
    }],
    story: [{
      date: {
        type: String,
      },
      content: {
        type: String,
      }
    }],
    victory: [{
      time: {
        type: String,
      },
      content: {
        type: String,
      }
    }],
    stages: [{
      time: {
        type: String,
      },
      desc: {
        type: String,
      },
      imageLink: {
        type: String
      },
      alt: {
        type: String,
      },
    }],
    prize: [{
      imageLink: {
        type: String,
      },
      prizeName: {
        type: String
      },
      prizeContent: {
        type: String
      },
      alt: {
        type: String,
      },
    }],
    someOfOtherImage: [{
      title: {
        type: String,
      },
      desc: {
        type: String,
      },
      imageLink: {
        type: String
      },
      alt: {
        type: String,
      },
    }],
  },
  contact: {
    numberPhoneContact: {
      type: String,
    },
    emailContact: {
      type: String,
    },
    addressContact: {
      type: String,
    },
    nameContact: {
      type: String,
    }
  },
  gpsLocation: {
    lat: {
      type: String,
    },
    long: {
      type: String,
    }
  },
  martyrs: [{
    name: {
      type: String,
    },
    birthday: {
      date: {
        type: String,
      },
      month: {
        type: String,
      },
      year: {
        type: String,
      }
    },
    dateOfDeath: {
      date: {
        type: String,
      },
      month: {
        type: String,
      },
      year: {
        type: String,
      }
    },
    desc: {
      type: String
    },
    imageLink: {
      type: String,
    },
    alt: {
      type: String,
    },
  }],
  posts: {
    title: {
      type: String,
    },
    content: {
      type: String
    },
    postedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users"
    }
  }
});

var qrCode = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  directTo: {
    required: true,
    type: String
  },
  imageQRLink: {
    required: false,
    type: String
  },
  isBanned: {
    required: true,
    type: Boolean,
    default: false
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date()
  },
});

var replyFromUsers = new mongoose.Schema({
  email: {
    required: true,
    type: String,
  },
  name: {
    required: true,
    type: String,
  },
  type: {
    required: true,
    type: String,
  },
  refDiachido: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'diachido'
  },
  refMevnah: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'mevnah'
  },
  districtsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  },
  title: {
    required: true,
    type: String,
  },
  content: {
    required: true,
    type: String,
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date(),
  },
  isReply: {
    required: true,
    type: Boolean,
    default: false,
  }
})

var cooperation = new mongoose.Schema({
  name: {
    required: true,
    type: String,
  },
  imageLink: {
    required: true,
    type: String,
  },
  alt: {
    type: String,
  },
  link: {
    required: true,
    type: String,
  },
  contactName: {
    required: false,
    type: String,
  },
  contactPhone: {
    required: false,
    type: String,
  },
  contactEmail: {
    required: false,
    type: String
  }
})

var notification = new mongoose.Schema({
  type: {
    required: true,
    type: String,
  },
  refDiachido: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'diachido'
  },
  refMevnah: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'mevnah'
  },
  districtsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "districts"
  },
  wardsID: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
    ref: "wards"
  },
  createdDate: {
    required: true,
    type: Date,
    default: new Date(),
  },
  description: [{
    name: {
      required: true,
      type: String,
    },
    content: {
      required: true,
      type: String,
    },
    timeShow: {
      required: true,
      type: Date,
    },
    timeEnd: {
      required: true,
      type: Date,
    },
    background: {
      imageLink: {
        type: String,
      },
      alt: {
        type: String,
      },
    }
  }]
})

var imageVisit = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String
  },
  slug: {
    type: String
  },
  imageLink: {
    type: String
  },
  createdDate: {
    type: Date,
    required: true,
    default: new Date()
  },
  showHomepage: {
    type: Boolean,
    default: false,
    required: true
  },
  postedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  refMevnah: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "mevnah"
  }
})

module.exports = {
  users: mongoose.model("users", users),
  objectUpload: mongoose.model("objectUpload", objectUpload),
  diachido: mongoose.model("diachido", diachido),
  forgetPassword: mongoose.model("forgetPassword", forgetPassword),
  qrCode: mongoose.model("qrCode", qrCode),
  districts: mongoose.model("districts", districts),
  dataNumber: mongoose.model("dataNumber", dataNumber),
  logs: mongoose.model("logs", logs),
  wards: mongoose.model("wards", wards),
  mevnah: mongoose.model("mevnah", mevnah),
  replyFromUsers: mongoose.model("replyFromUsers", replyFromUsers),
  cooperation: mongoose.model('cooperation', cooperation),
  notification: mongoose.model('notification', notification),
  imageVisit: mongoose.model('imageVisit', imageVisit)
};
